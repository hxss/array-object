<?php

namespace ArrayObject\Exceptions;

/**
 * Undefined property \Exception
 * thrown if the property of the object doesn't exist
 */
class UndefinedPropertyException extends UndefinedOffsetException {

	public function __construct(
		$message = '',
		$code = 0,
		\Exception $previous = null
	) {
		$this->message = $message ?: 'Undefined property: '
			. $this->getContext()['class'] . '::$' . $this->getContext()['args'][0];
	}
}
