<?php

namespace ArrayObject\Exceptions;

use ArrayObject\Exceptions\Traits\TraitExceptionTools;

/**
 * Undefined offset \Exception
 * thrown if the offset of the array doesn't exist.
 */
class UndefinedOffsetException extends \Exception {

	use TraitExceptionTools;

	public function __construct(
		$message = '',
		$code = 0,
		\Exception $previous = null
	) {
		$this->message = $message ?: 'Undefined offset: '
			. $this->getContext()['class'] . '::' . $this->getContext()['args'][0];
	}
}
