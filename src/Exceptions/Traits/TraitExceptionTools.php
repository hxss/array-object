<?php

namespace ArrayObject\Exceptions\Traits;

/**
 * Provides some methods for custom \Exception classes
 */
trait TraitExceptionTools {

	/**
	 * String representation of the exception
	 * @link https://www.php.net/manual/en/exception.tostring.php Description(php.net)
	 * @return string
	 */
	public function __toString() {
		$trace = $this->getContext();

		return get_class($this) . ': '
			. $this->getMessage() . ' In '
			. $trace['file'] . ':' . $trace['line'];
	}

	/**
	 * Returns context for \Exception
	 * @return array first backtrace item.
	 */
	protected function getContext() {
		return $this->getTrace()[0];
	}
}
