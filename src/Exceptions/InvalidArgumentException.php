<?php

namespace ArrayObject\Exceptions;

use ArrayObject\Contrib\ArgumentsTypeChecker;
use ArrayObject\Exceptions\Traits\TraitExceptionTools;

/**
 * Custom \InvalidArgumentException
 * based on \ArrayObject\Contrib\ArgumentsTypeChecker
 * thrown if an argument is not of the expected type.
 */
class InvalidArgumentException extends \InvalidArgumentException {

	use TraitExceptionTools;

	public $checker = null;

	private $_context = null;

	/**
	 * Constructs \ArrayObject\Exceptions\InvalidArgumentException based on \ArrayObject\Contrib\ArgumentsTypeChecker.
	 * @param ArgumentsTypeChecker $checker  The \ArrayObject\Contrib\ArgumentsTypeChecker created in the function.
	 * @param integer              $code     Code of exception.
	 * @param Throwable|null       $previous Previous \Throwable exception.
	 */
	public function __construct(
		ArgumentsTypeChecker $checker,
		$code = 0,
		Throwable $previous = null
	) {
		$this->checker = $checker;

		$messages = [];
		foreach ($this->checker->getMismatches() as $k => $mismatch) {
			$messages[] = 'Argument ' . ($k + 1)
				. ' passed to '
				. $this->getContext()['class'] . '::' . $this->getContext()['function']
				. ' must be of the type '
				. implode('|', $mismatch['validTypes'])
				. ', ' . gettype($mismatch['arg']) . ' given';
		}

		$this->message = implode('; ', $messages) . '.';
	}

	protected function getContext() {
		if ($this->_context == null) {
			$this->_context = $this->getTrace()[0]['class'] == ArgumentsTypeChecker::class
				? $this->getTrace()[1]
				: $this->getTrace()[0];
		}


		return $this->_context;
	}
}
