<?php

namespace ArrayObject\Traits;

/**
 * Provides smart static constructor
 * that can retrieve class instance as argument.
 */
trait TraitSelfStaticFactorySmart {

	use TraitSelfStaticFactory {
		create as createStraight;
	}

	/**
	 * Static method that can return first argument if its the class instance or call constructor with given arguments
	 * @return object
	 */
	public static function create($arg = []) {
		return $arg instanceof static
			? $arg
			: static::createStraight(...func_get_args());
	}
}
