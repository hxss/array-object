<?php

namespace ArrayObject\Traits;

use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerInterface;

/**
 * Implements \Zend\EventManager\EventManagerAwareInterface interface
 */
trait TraitEvents {

	/**
	 * @var Zend\EventManager\EventManager
	 */
	protected $eventsManager = null;

	/**
	 * Inject an EventManager instance
	 *
	 * @param  \Zend\EventManager\EventManagerInterface $eventManager
	 */
	public function setEventManager(EventManagerInterface $eventsManager) {
		$eventsManager->setIdentifiers([
			__CLASS__,
			get_called_class(),
		]);
		$this->eventsManager = $eventsManager;

		return $this;
	}

	/**
	 * Retrieve the event manager
	 *
	 * Lazy-loads an EventManager instance if none registered.
	 *
	 * @return \Zend\EventManager\EventManagerInterface
	 */
	public function getEventManager() {
		if ($this->eventsManager === null) {
			$this->setEventManager(new EventManager());
		}

		return $this->eventsManager;
	}
}
