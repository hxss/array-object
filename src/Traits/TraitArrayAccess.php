<?php

namespace ArrayObject\Traits;

use ArrayObject\Contrib\ArrayObjectItem;
use ArrayObject\Exceptions\UndefinedOffsetException;

/**
 * Implements ArrayAccess interface
 * @link https://www.php.net/manual/en/class.arrayaccess.php Description(php.net)
 */
trait TraitArrayAccess {

	/**
	 * Original array
	 * @var array $items
	 */

	/**
	 * This method is executed when using isset() or empty() on objects implementing ArrayAccess.
	 * @link https://www.php.net/manual/en/arrayaccess.offsetexists.php Description(php.net)
	 * @param  mixed $name An offset to check for
	 * @return boolean
	 */
	public function offsetExists($name) {
		return isset($this->items[$name]);
	}

	/**
	 * Unsets an offset.
	 * @link https://www.php.net/manual/en/arrayaccess.offsetunset.php Description(php.net)
	 * @param  mixed $name The offset to unset.
	 */
	public function offsetUnset($name) {
		$valueRemoved = $this->items[$name] ?? null;

		unset($this->items[$name]);

		$this->getEventManager()->trigger(
			static::EVENT_ITEM_REMOVED,
			$this,
			new ArrayObjectItem($name, $valueRemoved)
		);
	}

	/**
	 * Returns the value at specified offset
	 * @link https://www.php.net/manual/en/arrayaccess.offsetget.php Description(php.net)
	 * @param  mixed $name The offset to retrieve.
	 * @return mixed
	 */
	public function offsetGet($name) {
		if (!array_key_exists($name, $this->items)) {
			throw new UndefinedOffsetException();
		}

		return $this->items[$name];
	}

	/**
	 * Assigns a value to the specified offset
	 * @link https://www.php.net/manual/en/arrayaccess.offsetset.php Description(php.net)
	 * @param  mixed $name  The offset to assign the value to.
	 * @param  mixed $value The value to set.
	 */
	public function offsetSet($name, $value) {
		if ($name === null) {
			$this->items[] = $value;
		} else {
			$this->items[$name] = $value;
		}

		$this->getEventManager()->trigger(
			static::EVENT_ITEM_ADDED,
			$this,
			new ArrayObjectItem($name, $value)
		);
	}
}
