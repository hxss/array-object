<?php

namespace ArrayObject\Traits;

/**
 * Implements Iterator interface
 * @link https://www.php.net/manual/en/class.iterator.php Description(php.net)
 */
trait TraitIterator {

	/**
	 * Original array
	 * @var array $items
	 */

	/**
	 * Return[ or update value of] the current element
	 * @link https://www.php.net/manual/en/iterator.current.php Description(php.net)
	 * @param  mixed $val (optional) New value for current element.
	 * @return mixed
	 */
	public function current($val = null) {
		if (func_num_args()) {
			$this->items[$this->key()] = $val;
		}

		return current($this->items);
	}

	/**
	 * Return the key of the current element
	 * @link https://www.php.net/manual/en/iterator.key.php Description(php.net)
	 * @return scalar
	 */
	public function key() {
		return key($this->items);
	}

	/**
	 * Move forward to next element
	 * @link https://www.php.net/manual/en/iterator.next.php Description(php.net)
	 */
	public function next() {
		return next($this->items);
	}

	/**
	 * Rewind the Iterator to the first element
	 * @link https://www.php.net/manual/en/iterator.rewind.php Description(php.net)
	 */
	public function rewind() {
		reset($this->items);
	}

	/**
	 * Checks if current position is valid
	 * @link https://www.php.net/manual/en/iterator.valid.php Description(php.net)
	 * @return bool
	 */
	public function valid() {
		return key($this->items) !== null;
	}
}
