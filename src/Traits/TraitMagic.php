<?php

namespace ArrayObject\Traits;

use ArrayObject\Exceptions\UndefinedOffsetException;
use ArrayObject\Exceptions\UndefinedPropertyException;

/**
 * Implements some magic methods based on \ArrayObject\Traits\TraitArrayAccess
 * @link https://www.php.net/manual/en/language.oop5.magic.php Description(php.net)
 */
trait TraitMagic {

	use TraitArrayAccess;

	/**
	 * Magic method triggered by calling isset() or empty() on inaccessible properties.
	 * @link https://www.php.net/manual/en/language.oop5.overloading.php#object.isset Description(php.net)
	 * @param  string  $name the name of the property being interacted with.
	 * @return boolean
	 */
	public function __isset($name) {
		return $this->offsetExists($name);
	}

	/**
	 * Magic method invoked when unset() is used on inaccessible properties.
	 * @link https://www.php.net/manual/en/language.oop5.overloading.php#object.unset Description(php.net)
	 * @param string $name the name of the property being interacted with.
	 */
	public function __unset($name) {
		$this->offsetUnset($name);
	}

	/**
	 * Magic method utilized for reading data from inaccessible properties.
	 * @link https://www.php.net/manual/en/language.oop5.overloading.php#object.get Description(php.net)
	 * @param  string $name the name of the property being interacted with.
	 * @return mixed
	 */
	public function __get($name) {
		try {
			return $this->offsetGet($name);
		} catch (UndefinedOffsetException $e) {
			throw new UndefinedPropertyException();
		}
	}

	/**
	 * Magic method run when writing data to inaccessible properties.
	 * @link https://www.php.net/manual/en/language.oop5.overloading.php#object.set Description(php.net)
	 * @param string $name  the name of the property being interacted with.
	 * @param mixed  $value The value to set.
	 */
	public function __set($name, $value) {
		$this->offsetSet($name, $value);
	}

	/**
	 * Returns an array with the names of all variables of that object that should be serialized.
	 * @link https://www.php.net/manual/en/language.oop5.magic.php#object.sleep Description(php.net)
	 * @return array
	 */
	public function __sleep() {
		return ['items'];
	}

	/**
	 * Magic method called for classes exported by var_export()
	 * @link https://www.php.net/manual/en/language.oop5.magic.php#language.oop5.magic.set-state Description(php.net)
	 * @param  array $props array containing exported properties
	 * @return object
	 */
	public static function __set_state($props) {
		return new static($props['items']);
	}

	/**
	 * This method is called by var_dump() when dumping an object to get the properties that should be shown.
	 * @return array
	 */
	public function __debugInfo() {
		return $this->items;
	}

	/**
	 * Tunes new cloned instance;
	 * @link www.php.net/manual/en/language.oop5.cloning.php Description(php.net)
	 */
	public function __clone() {
		$this->eventsManager = null;
	}
}
