<?php

namespace ArrayObject\Traits;

/**
 * Implements Countable interface
 * @link https://www.php.net/manual/en/class.countable.php Description(php.net)
 */
trait TraitCountable {

	/**
	 * Original array
	 * @var array $items
	 */

	/**
	 * Count elements of an object
	 * @link https://www.php.net/manual/en/countable.count.php Description(php.net)
	 * @return int
	 */
	public function count() {
		return count($this->items);
	}
}
