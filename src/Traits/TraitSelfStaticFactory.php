<?php

namespace ArrayObject\Traits;

/**
 * Provides static \ArrayObject\Traits\TraitSelfStaticFactory::create method
 * for calling class constructor.
 */
trait TraitSelfStaticFactory {

	/**
	 * Static method that call constructor with given arguments
	 * @return object
	 */
	public static function create() {
		return new static(...func_get_args());
	}
}
