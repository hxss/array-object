<?php

namespace ArrayObject\Contrib;

use ArrayObject\Exceptions\InvalidArgumentException;
use ArrayObject\Traits\TraitSelfStaticFactory;

/**
 * Allows to validate function arguments
 * with multiple or scalar allowed types like in PHP 7.
 */
class ArgumentsTypeChecker {

	use TraitSelfStaticFactory;

	const TYPE_MIXED = 'mixed';

	private $rules = null;
	private $mismatches = null;

	/**
	 * @param array  $arguments  array of arguments given in the function.
	 * @param array  $validTypes variable count of arrays that contains valid scalar types or classes names. Each array describes one argument types.
	 */
	public function __construct(array $arguments, array ...$validTypes) {
		$length = min(count($arguments), count($validTypes));

		$this->rules = array_map(
			function ($arg, $types) {
				return [
					'arg' => $arg,
					'validTypes' => $types,
				];
			},
			array_slice($arguments, 0, $length),
			array_slice($validTypes, 0, $length)
		);
	}

	/**
	 * Checks if the arguments match their types.
	 * @return boolean
	 */
	public function isValid() {
		return !$this->getMismatches();
	}

	/**
	 * Checks if the arguments match their types and throw the \ArrayObject\Exceptions\InvalidArgumentException if not.
	 */
	public function check() {
		if (!$this->isValid()) {
			throw new InvalidArgumentException($this);
		}
	}

	/**
	 * Generates array with mismatches of arguments and their valid types.
	 * @return array
	 */
	public function getMismatches() {
		if ($this->mismatches == null) {
			$this->mismatches = array_filter(
				$this->rules,
				[$this, 'isInvalid']
			);
		}

		return $this->mismatches;
	}

	private function isInvalid($rule) {
		return !$this->validateRule($rule);
	}

	private function validateRule($rule) {
		$arg = $rule['arg'];

		foreach ($rule['validTypes'] as $type) {
			if (
				$type == ArgumentsTypeChecker::TYPE_MIXED
				|| gettype($arg) == $type
				|| is_a($arg, $type)
				|| (
					function_exists($is_type = 'is_' . $type)
					&& $is_type($arg)
				)
			) {
				return true;
			}
		}
	}
}
