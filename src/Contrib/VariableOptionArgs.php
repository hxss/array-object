<?php

namespace ArrayObject\Contrib;

use ArrayObject\ArrayObject;
use ArrayObject\Contrib\ArgumentsTypeChecker;
use ArrayObject\Traits\TraitSelfStaticFactory;

/**
 * Check and parse args of a compare function
 */
class VariableOptionArgs {

	use TraitSelfStaticFactory;

	/**
	 * Variable-length array list
	 * @var ArrayObject
	 */
	public $others = null;

	/**
	 * Optional argument
	 * @var mixed
	 */
	public $option = null;

	/**
	 * Option valid types list.
	 * @var ArrayObject
	 */
	private $optionTypes = null;

	public function __construct(array $optionTypes) {
		$this->optionTypes = new ArrayObject($optionTypes);
	}

	/**
	 * Check and parse args of a compare function
	 * @param  array|ArrayObject $others,... Arrays to compare against.
	 * @param  ArrayObject::FLAG_KEY|ArrayObject::FLAG_ASSOC|callable $option (optional) ArrayObject flag or callable function.
	 */
	public function parse() {
		$args = new ArrayObject(func_get_args(), 1);

		$types = ArrayObject
			::fill(ArrayObject::TYPE_ARRAY, $args->count())
			->recursive();

		$args->count() > 1
			&& $types->last()->push(...$this->optionTypes);

		ArgumentsTypeChecker
			::create(
				$args->items(),
				...$types->items(true)
			)
			->check();

		$args->count() > 1
			&& $this->option = ArgumentsTypeChecker
				::create(
					[$args->last()],
					$this->optionTypes->items()
				)
				->isValid()
					? $args->pop()
					: null;

		$args->items(true);
		$this->others = $args;

		return $this;
	}
}
