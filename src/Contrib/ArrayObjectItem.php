<?php

namespace ArrayObject\Contrib;

/**
 * \ArrayObject\ArrayObject array item desctription.
 */
class ArrayObjectItem {

	/**
	 * Array item key.
	 * @var mixed
	 */
	public $key = null;

	/**
	 * Array item value.
	 * @var mixed
	 */
	public $value = null;

	public function __construct($key, $value) {
		$this->key = $key;
		$this->value = $value;
	}
}
