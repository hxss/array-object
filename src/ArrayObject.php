<?php

namespace ArrayObject;

use ArrayObject\Contrib\ArgumentsTypeChecker;
use ArrayObject\Contrib\ArrayObjectItem;
use ArrayObject\Contrib\VariableOptionArgs;
use ArrayObject\Exceptions\InvalidArgumentException;
use ArrayObject\Traits\TraitCountable;
use ArrayObject\Traits\TraitEvents;
use ArrayObject\Traits\TraitIterator;
use ArrayObject\Traits\TraitMagic;
use ArrayObject\Traits\TraitSelfStaticFactory;
use ArrayObject\Traits\TraitSelfStaticFactorySmart;
use Zend\EventManager\EventManagerAwareInterface;

/**
 * Provides object oriented access to array.
 * Implements ArrayAccess, Iterator, Countable interfaces
 * and magic methods for access to array.
 */
class ArrayObject implements
	\ArrayAccess,
	\Countable,
	\Iterator,
	EventManagerAwareInterface
{

	use TraitMagic;
	use TraitCountable;
	use TraitIterator;
	use TraitSelfStaticFactorySmart;
	use TraitEvents;

	/**
	 * Use reverse order on sorting.
	 */
	const SORT_REVERSE   = 0b00010000;

	/**
	 * Use keys in function.
	 */
	const FLAG_KEY       = 0b00100000;

	/**
	 * Use index check in function.
	 */
	const FLAG_ASSOC     = 0b01000000;

	/**
	 * Execute method recursively.
	 */
	const FLAG_RECURSIVE = 0b10000000;

	/**
	 * Main types accepted as arrays.
	 */
	const TYPE_ARRAY = [
		'array',
		ArrayObject::class,
		\Traversable::class
	];

	/**
	 * Event triggered on $items array update.
	 */
	const EVENT_ITEMS_UPDATED = 'itemsUpdated';

	/**
	 * Event triggered on item added into $items.
	 */
	const EVENT_ITEM_ADDED    = 'itemAdded';

	/**
	 * Event triggered on item removed from $items.
	 */
	const EVENT_ITEM_REMOVED  = 'itemRemoved';

	/**
	 * Evenet triggered on $items array sorting.
	 */
	const EVENT_SORT          = 'sort';

	/**
	 * Original array
	 * @var array
	 */
	protected $items = [];

	/**
	 * Constructs the class instance
	 * @param array|\Traversable $items array to interaction
	 * @param bool|int  $recursive (optional) Specify is array items of new object
	 * should be converted to ArrayObject too or int level of recursion.
	 */
	public function __construct($items = [], $recursive = false) {
		ArgumentsTypeChecker
			::create(
				func_get_args(),
				['array', \Traversable::class],
				['bool', 'int']
			)
			->check();

		$this->items = $items instanceof \Traversable
			? iterator_to_array($items)
			: $items;

		$this->recursive($recursive);

		$this->rewind();
	}

	/**
	 * Finds whether a variables is an array
	 * @param  mixed  $vars The variables being evaluated
	 * @return boolean      True if all vars is array or instanceof ArrayObject
	 */
	public static function isArray(...$vars) {
		return ArgumentsTypeChecker
			::create(
				$vars,
				...array_fill(
					0,
					count($vars),
					static::TYPE_ARRAY
				)
			)
			->isValid();
	}

	/**
	 * Creates an array by using one array for keys and another for its values
	 * @link https://www.php.net/manual/en/function.array-combine.php Description(php.net)
	 * @param  array|ArrayObject $keys   Array of keys to be used. Illegal values for key will be converted to string.
	 * @param  array|ArrayObject $values Array of values to be used
	 * @return ArrayObject
	 */
	public static function combine($keys, $values) {
		ArgumentsTypeChecker::create(
			func_get_args(), static::TYPE_ARRAY, static::TYPE_ARRAY
		)->check();

		return new static(
			array_combine(
				static::create($keys)->items(),
				static::create($values)->items()
			)
		);
	}

	/**
	 * Fill an array with values
	 * @link https://www.php.net/manual/en/function.array-fill.php Description(php.net)
	 * @param  mixed                 $value  Value to use for filling.
	 * @param  array|ArrayObject|int $rule   Array of values that will be used as keys OR Number of elements to insert(>=0).
	 * @param  int                   $offset (optional) The first index of the returned array(only used with number-rule).
	 * @return ArrayObject
	 */
	public static function fill($value, $rule, $offset = 0) {
		return static::isArray($rule)
			? static::fillKeys($value, $rule)
			: static::fillDefault(...func_get_args());
	}

	/**
	 * Fill an array with values
	 * @link https://www.php.net/manual/en/function.array-fill.php Description(php.net)
	 * @param  mixed  $value  Value to use for filling.
	 * @param  int    $num    Number of elements to insert. Must be greater than or equal to zero.
	 * @param  int    $offset The first index of the returned array.
	 * @return ArrayObject
	 */
	protected static function fillDefault($value, $num, $offset = 0) {
		ArgumentsTypeChecker
			::create(
				func_get_args(),
				[ArgumentsTypeChecker::TYPE_MIXED],
				['int'],
				['int']
			)
			->check();

		return new static(
			array_fill($offset, $num, $value)
		);
	}

	/**
	 * Fill an array with values, specifying keys
	 * @link https://www.php.net/manual/en/function.array-fill-keys.php Description(php.net)
	 * @param  array|ArrayObject $keys  Array of values that will be used as keys.
	 * @param  mixed             $value Value to use for filling
	 * @return ArrayObject
	 */
	protected static function fillKeys($value, $keys) {
		ArgumentsTypeChecker
			::create(
				func_get_args(),
				[ArgumentsTypeChecker::TYPE_MIXED],
				static::TYPE_ARRAY
			)
			->check();

		return new static(
			array_fill_keys($keys, $value)
		);
	}

	/**
	 * Create an array containing a range of elements.
	 * @link https://www.php.net/manual/en/function.range.php Description(php.net)
	 * @param  mixed  $start First value of the sequence.
	 * @param  mixed  $end   The sequence is ended upon reaching the end value.
	 * @param  integer $step (optional) If a step value is given,
	 * it will be used as the increment between elements in the sequence.
	 * @return ArrayObject
	 */
	public static function range($start, $end, $step = 1) {
		return new static(
			range(...func_get_args())
		);
	}

	/**
	 * Recursively convert all array items from array to ArrayObject.
	 * @return self
	 */
	public function recursive($level = true) {
		ArgumentsTypeChecker
			::create(
				func_get_args(),
				['bool', 'int']
			)
			->check();

		if ($level) {
			$this->walk(function(&$item) use ($level) {
				if ($this->isArray($item)) {
					$item = static::create(
						$item,
						is_int($level) ? $level - 1 : $level
					);
				}
			});
		}

		return $this;
	}

	/**
	 * Returns an array used in object.
	 * @param bool  $recursive (optional) Specify is sub-ArrayOject's should be
	 * converted to arrays.
	 */
	public function items($recursive = false) {
		if ($recursive) {
			$this->walk(function(&$item) {
				if ($item instanceof ArrayObject) {
					$item = $item->items(true);
				}
			});
		}

		return $this->items;
	}

	/**
	 * Checks if the object equal to $other array
	 * @param  array|ArrayObject|\Traversable $other
	 * @return bool
	 */
	public function eq($other) {
		return $this->items === static::create($other)->items;
	}

	public function isIndexed() {
		return !($count = $this->count())
			|| $this->keys()->eq(
				static::range(0, $count - 1)
			);
	}

	public function isAssociative() {
		return !$this->isIndexed();
	}

	/**
	 * Changes the case of all keys in an array
	 * @link https://www.php.net/manual/en/function.array-change-key-case.php Description(php.net)
	 * @param  int $case (optional) Either CASE_UPPER or CASE_LOWER (default).
	 * @return ArrayObject an array with its keys lower or uppercased
	 */
	public function changeKeyCase($case = CASE_LOWER) {
		return new static(
			array_change_key_case($this->items, $case)
		);
	}

	/**
	 * Split an array into chunks
	 * @link https://www.php.net/manual/en/function.array-chunk.php Description(php.net)
	 * @param  int     $size          The size of each chunk
	 * @param  boolean $preserve_keys (optional) When set to TRUE keys will be preserved. Default is FALSE which will reindex the chunk numerically
	 * @return ArrayObject[]          Returns a multidimensional numerically indexed array, starting with zero, with each dimension containing $size elements.
	 */
	public function chunk($size, $preserve_keys = false) {
		$chunks = [];

		foreach (array_chunk($this->items, $size, $preserve_keys) as $chunk) {
			$chunks[] = new static($chunk);
		}

		return new static($chunks);
	}

	/**
	 * Return the values from a single column in the input array.
	 * @link https://www.php.net/manual/en/function.array-column.php Description(php.net)
	 * @param  int|string      $columnKey The column of values to return.
	 * @param  int|string|null $indexKey  (optional) The column to use as the index/keys for the returned array.
	 * @return ArrayObject     Returns an array of values representing a single column from the input array.
	 */
	public function column($columnKey, $indexKey = null) {
		return new static(
			array_column($this->items(true), $columnKey, $indexKey)
		);
	}

	/**
	 * Counts all the values of an array.
	 * @link https://www.php.net/manual/en/function.array-count-values.php Description(php.net)
	 * @return ArrayObject Returns an associative array of values from array as keys and their count as value.
	 */
	public function countValues() {
		return new static(
			array_count_values($this->items())
		);
	}

	/**
	 * Computes the difference of arrays.
	 * Depending to $option it can additionaly check index, only keys
	 * or using callback function.
	 * @link https://www.php.net/manual/en/function.array-diff.php Description(php.net)
	 * @param  array|ArrayObject|\Traversable $others,... Arrays to compare against.
	 * @param  ArrayObject::FLAG_KEY|ArrayObject::FLAG_ASSOC|callable $option (optional) ArrayObject flag or callable function.
	 * that specify how to compare arrays.
	 * @return ArrayObject
	 */
	public function diff() {
		$compareArgs = VariableOptionArgs::create(['int', 'callable'])
			->parse(...func_get_args());

		if ($compareArgs->option === static::FLAG_KEY) {
			return $this->diffKey(...$compareArgs->others);
		} elseif ($compareArgs->option === static::FLAG_ASSOC) {
			return $this->diffAssoc(...$compareArgs->others);
		} elseif ($compareArgs->option === static::FLAG_RECURSIVE) {
			return $this->diffRecursive(...$compareArgs->others);
		} elseif (is_callable($compareArgs->option)) {
			return $this->diffUser($compareArgs->others, $compareArgs->option);
		}

		return $this->diffDefault($compareArgs->others);
	}

	/**
	 * Computes the difference of arrays.
	 * @link https://www.php.net/manual/en/function.array-diff.php Description(php.net)
	 * @param  ArrayObject $others Arrays to compare against.
	 * @return ArrayObject
	 */
	protected function diffDefault(ArrayObject $others) {
		return new static(
			array_diff($this->items, ...$others->items)
		);
	}

	/**
	 * Computes the difference of arrays with additional index check.
	 * @link https://www.php.net/manual/en/function.array-diff-assoc.php Description(php.net)
	 * @param  array|ArrayObject|\Traversable $others,... Arrays to compare against.
	 * @return ArrayObject
	 */
	public function diffAssoc(...$others) {
		return new static(
			array_diff_assoc(
				$this->items,
				...static::create($others, true)->items(true)
			)
		);
	}

	/**
	 * Computes the difference of arrays using keys for comparison.
	 * @link https://www.php.net/manual/en/function.array-diff-key.php Description(php.net)
	 * @param  array|ArrayObject|\Traversable $others,... Arrays to compare against.
	 * @return ArrayObject
	 */
	public function diffKey(...$others) {
		return new static(
			array_diff_key(
				$this->items,
				...static::create($others, true)->items(true)
			)
		);
	}

	/**
	 * Recursively computes the difference of arrays and sub-arrays.
	 * @param  array|ArrayObject|\Traversable $others,... Arrays to compare against.
	 * @return ArrayObject
	 */
	public function diffRecursive(...$others) {
		$others = static::create($others, true);

		$diff = new static([]);

		foreach ($this as $k => $v) {
			$otherValues = $others->column($k);

			if (
				$otherValues->count() != $others->count()
				|| !static::isArray($v)
				&& $otherValues
					->filter(function ($ok, $ov) use ($v) {
						return $v != $ov;
					})
					->count() > 0
				|| static::isArray($v)
				&& $otherValues
					->filter(function ($ok, $ov) use ($v) {
						return !static::isArray($ov);
					})
					->count() > 0
				|| static::isArray($v)
				&& (
					$v = static::create($v)
						->diffRecursive(...$otherValues)
						->items()
				)
			) {
				$diff[$k] = $v;
			}
		}

		return $diff;
	}

	/**
	 * Computes the difference of arrays which is performed by a user supplied callback function.
	 * @link https://www.php.net/manual/en/function.array-diff-key.php Description(php.net)
	 * @param  ArrayObject $others Arrays to compare against.
	 * @param  callable $userFunc custom compare function.
	 * @return ArrayObject
	 */
	protected function diffUser(ArrayObject $others, callable $userFunc) {
		return $this->filter(function ($k, $v) use ($others, $userFunc) {
			foreach ($others as $other) {
				foreach ($other as $ok => $ov) {
					if ($userFunc($k, $v, $ok, $ov) == 0) {
						return false;
					}
				}
			}

			return true;
		});
	}

	/**
	 * Filters elements of an array using a callback function
	 * @link https://www.php.net/manual/en/function.array-filter.php Description(php.net)
	 * @param  callable|null $callback The callback function to use.
	 * Always receive 2 arguments: key and value in this order.
	 * @return ArrayObject
	 */
	public function filter(callable $callback = null) {
		$callbackArgs = [];
		if ($callback) {
			$callbackArgs[0] = function($v, $k) use($callback) {
				return $callback($k, $v);
			};
			$callbackArgs[1] = ARRAY_FILTER_USE_BOTH;
		}

		return new static(
			array_filter($this->items, ...$callbackArgs)
		);
	}

	/**
	 * Exchanges all keys with their associated values in an array
	 * @link https://www.php.net/manual/en/function.array-flip.php Description(php.net)
	 * @return ArrayObject
	 */
	public function flip() {
		return new static(
			array_flip($this->items)
		);
	}

	/**
	 * Generates new array by user generator.
	 * @param  callable                       $callback Generator function to run for each element. Takes $key(s) and $value(s).
	 * @param  array|ArrayObject|\Traversable $others   (optional) Additional associative array of other arrays to run through the generator
	 * @param  int                            $flags    (optional) MultipleIterator flags. MultipleIterator uses with $others arrays.
	 * @return ArrayObject
	 */
	public function generate(callable $callback, array $others = null, $flags = null) {
		$others = static::create($others ?: [], 1);

		ArgumentsTypeChecker::create(
			$others->items,
			...static
				::fill(static::TYPE_ARRAY, $others->count())
				->items
		)->check();

		$isMultiple = (int)(bool)$others->count();
		$iterator = $isMultiple
			? $this->multipleIterator($others, $flags)
			: $this;

		$result = static::create();
		foreach ($iterator as $key => $value) {
			$args = static::create([$key, $value], $isMultiple);
			$result[] = static::create(
				$callback(...$args->items())
			);
		}

		return $this->combineGenerated($result);
	}

	private function combineGenerated(ArrayObject $result) {
		$isAssoc = $result
			->map(function ($generated) {
				return $generated->isAssociative();
			})
			->sum();

		$method = $isAssoc ? 'replace' : 'merge';

		return $result->count()
			? $result->shift()->{$method}(...$result)
			: $result;
	}

	private function multipleIterator($others, $flags)
	{
		$iterator = new \MultipleIterator($flags);
		$iterator->attachIterator($this, 'this');

		foreach ($others as $key => $other) {
			$iterator->attachIterator($other, $key);
		}

		return $iterator;
	}

	/**
	 * Checks if a value exists in an array.
	 * @link https://www.php.net/manual/en/function.in-array.php Description(php.net)
	 * @param  mixed   $needle The searched value.
	 * @param  boolean $strict (optional) Requires to use strict checking(===).
	 * @return boolean
	 */
	public function has($needle, $strict = false) {
		return in_array($needle, $this->items, $strict);
	}

	/**
	 * Computes the intersection of arrays
	 * Depending to $option it can additionaly check index, only keys
	 * or using callback function.
	 * @link https://www.php.net/manual/en/function.array-intersect.php Description(php.net)
	 * @param  array|ArrayObject|\Traversable $others,... Arrays to compare against.
	 * @param  ArrayObject::FLAG_KEY|ArrayObject::FLAG_ASSOC|callable $option (optional) ArrayObject flag or callable function.
	 * that specify how to compare arrays.
	 * @return ArrayObject
	 */
	public function intersect() {
		$compareArgs = VariableOptionArgs::create(['int', 'callable'])
			->parse(...func_get_args());

		if ($compareArgs->option === static::FLAG_KEY) {
			return $this->intersectKey(...$compareArgs->others);
		} elseif ($compareArgs->option === static::FLAG_ASSOC) {
			return $this->intersectAssoc(...$compareArgs->others);
		} elseif (is_callable($compareArgs->option)) {
			return $this->intersectUser(
				$compareArgs->others,
				$compareArgs->option
			);
		}

		return $this->intersectDefault($compareArgs->others);
	}

	/**
	 * Computes the intersection of arrays
	 * @link https://www.php.net/manual/en/function.array-intersect.php Description(php.net)
	 * @param  ArrayObject $others Arrays to compare against
	 * @return ArrayObject
	 */
	protected function intersectDefault(ArrayObject $others) {
		return new static(
			array_intersect($this->items, ...$others->items)
		);
	}

	/**
	 * Computes the intersection of arrays with additional index check
	 * @link https://www.php.net/manual/en/function.array-intersect-assoc.php Description(php.net)
	 * @param  array|ArrayObject|\Traversable $others,... Arrays to compare against
	 * @return ArrayObject
	 */
	public function intersectAssoc(...$others) {
		return new static(
			array_intersect_assoc(
				$this->items,
				...static::create($others, 1)->items(true)
			)
		);
	}

	/**
	 * Computes the intersection of arrays using keys for comparison
	 * @link https://www.php.net/manual/en/function.array-intersect-key.php Description(php.net)
	 * @param  array|ArrayObject|\Traversable $others,... Arrays to compare against
	 * @return ArrayObject
	 */
	public function intersectKey(...$others) {
		return new static(
			array_intersect_key(
				$this->items,
				...static::create($others, 1)->items(true)
			)
		);
	}

	/**
	 * Computes the intersection of arrays which is performed by a user supplied callback function.
	 * @link https://www.php.net/manual/en/function.array-intersect-uassoc.php Description(php.net)
	 * @param  ArrayObject $others Arrays to compare against.
	 * @return ArrayObject
	 */
	protected function intersectUser(ArrayObject $others, callable $userFunc) {
		$intersect = $this->items;

		foreach ($this->items as $k => $v) {
			foreach ($others as $other) {
				$itemExist = false;
				foreach ($other as $ok => $ov) {
					$itemExist |= !$userFunc($k, $v, $ok, $ov);
				}

				if (!$itemExist) {
					unset($intersect[$k]);
					break;
				}
			}
		}

		return new static($intersect);
	}

	/**
	 * Checks if the given key or index exists in the array
	 * @link https://www.php.net/manual/en/function.array-key-exists.php Description(php.net)
	 * @param  mixed $name Value to check
	 * @return bool
	 */
	public function keyExist($name) {
		return array_key_exists($name, $this->items);
	}

	/**
	 * Return all the keys or a subset of the keys of an array
	 * @link https://www.php.net/manual/en/function.array-keys.php Description(php.net)
	 * @param  mixed   $searchValue (optional) If specified, then only keys containing these values are returned.
	 * @param  boolean $strict      (optional) Determines if strict comparison (===) should be used during the search.
	 * @return ArrayObject
	 */
	public function keys($searchValue = null, $strict = false) {
		return new static(
			array_keys($this->items, ...func_get_args())
		);
	}

	/**
	 * Applies the callback to the elements of the given arrays
	 * @link https://www.php.net/manual/en/function.array-map.php Description(php.net)
	 * @param  callable          $callback   Callback function to run for each element in each array.
	 * @param  array|ArrayObject|\Traversable $others,... Additional variable list of array arguments to run through the callback function.
	 * @return ArrayObject
	 */
	public function map(callable $callback, ...$others) {
		$others = static::create($others, 1);

		ArgumentsTypeChecker::create(
			$others->items,
			...static
				::fill(static::TYPE_ARRAY, $others->count())
				->items
		)->check();

		return new static(
			array_map($callback, $this->items, ...$others->items(true))
		);
	}

	/**
	 * Merge one or more arrays
	 * @link https://www.php.net/manual/en/function.array-merge.php Description(php.net)
	 * @param  array|ArrayObject|\Traversable $others,... Variable list of arrays to merge
	 * @param  bool              $recursive  (optional) Flag determine that
	 * merge should be done recursively
	 * @return ArrayObject
	 */
	public function merge() {
		$compareArgs = VariableOptionArgs::create(['bool'])
			->parse(...func_get_args());

		$others = $compareArgs->others;
		$recursive = $compareArgs->option;

		return new static(
			$recursive
				? array_merge_recursive($this->items, ...$others)
				: array_merge($this->items, ...$others)
		);
	}

	/**
	 * Sort multiple or multi-dimensional arrays.
	 * Calls array_multisort with passed arguments prepended with current array.
	 * All array|ArrayObject args will used in multisorting,
	 * but only ArrayObject args will be able to save changes.
	 * @link https://www.php.net/manual/en/function.array-multisort.php Description(php.net)
	 * @param  mixed $sortOrder (optional) The order used to sort the previous array argument.
	 * @param  mixed $sortFlags (optional) Sort options for the previous array argument.
	 * @param  mixed $other,... (optional) More arrays, optionally followed by sort order and flags.
	 * @return self
	 */
	public function multisort($sortOrder = SORT_ASC, $sortFlags = SORT_REGULAR) {
		$objects = static::create();
		$args = static::create(func_get_args())
			->prepend($this)
			->walk(function (&$arg) use ($objects) {
				if (is_array($arg)) {
					$arg = new ArrayObject($arg);
				}

				if ($arg instanceof ArrayObject) {
					$objects->push($arg);
					$arg = $arg->items;
				}
			})
			->items();

		array_multisort(...$args);

		foreach ($args as $arg) {
			if (is_array($arg)) {
				$objects->current()->items = $arg;
				$objects->next();
			}
		}

		$this->getEventManager()->trigger(
			static::EVENT_SORT,
			$this
		);

		return $this;
	}

	/**
	 * Pad array to the specified length with a value.
	 * @link https://www.php.net/manual/en/function.array-pad.php Description(php.net)
	 * @param  int    $size  New size of the array.
	 * @param  mixed  $value Value to pad if array is less than $size.
	 * @return ArrayObject Returns a copy of the array padded to size
	 * specified by size with value value.
	 * If size is positive then the array is padded on the right,
	 * if it's negative then on the left.
	 * If the absolute value of size is less than or equal to
	 * the length of the array then no padding takes place.
	 */
	public function pad($size, $value) {
		return new static(
			array_pad($this->items, $size, $value)
		);
	}

	/**
	 * Pop the element off the end of array
	 * @link https://www.php.net/manual/en/function.array-pop.php Description(php.net)
	 * @return mixed
	 */
	public function pop() {
		$keyRemoved = $this->lastKey();
		$valueRemoved = array_pop($this->items);

		$this->getEventManager()->trigger(
			static::EVENT_ITEM_REMOVED,
			$this,
			new ArrayObjectItem($keyRemoved, $valueRemoved)
		);

		return $valueRemoved;
	}

	/**
	 * Calculate the product of values in an array
	 * @link https://www.php.net/manual/en/function.array-product.php Description(php.net)
	 * @return int
	 */
	public function product() {
		return array_product($this->items);
	}

	/**
	 * Push one or more elements onto the end of array
	 * @link https://www.php.net/manual/en/function.array-push.php Description(php.net)
	 * @param  mixed $values,... The values to push onto the end of the array.
	 * @return int               Returns the new number of elements in the array
	 */
	public function push(...$values) {
		$newCount = array_push($this->items, ...$values);

		foreach ($this->slice(-count($values), null, true) as $key => $value) {
			$this->getEventManager()->trigger(
				static::EVENT_ITEM_ADDED,
				$this,
				new ArrayObjectItem($key, $value)
			);
		}

		return $newCount;
	}

	/**
	 * Append one or more elements onto the end of array
	 * @param  mixed $values,... The values to append onto the end of the array.
	 * @return self
	 */
	public function append(...$values) {
		$this->push(...$values);

		return $this;
	}

	/**
	 * Returns associated array with one or more random items of an array.
	 * @param  int $num how many entries should be picked.
	 * @return ArrayObject
	 */
	public function rand($num = 1) {
		$rand = new static();

		foreach ($this->randKeys($num) as $key) {
			$rand[$key] = $this->items[$key];
		}

		return $rand;
	}

	/**
	 * Pick one or more random keys out of an array.
	 * @link https://www.php.net/manual/en/function.array-rand.php Description(php.net)
	 * @param  int $num how many entries should be picked.
	 * @return ArrayObject  An array of keys for the random entries.
	 */
	public function randKeys($num = 1) {
		return new static(
			(array)array_rand($this->items, $num)
		);
	}

	/**
	 * Iteratively reduce the array to a single value using a callback function.
	 * @link https://www.php.net/manual/en/function.array-reduce.php Description(php.net)
	 * @param  callable $callback
	 * @param  mixed   $initial  (optional) If the optional initial is available, it will be used at the beginning of the process, or as a final result in case the array is empty.
	 * @return mixed|null
	 */
	public function reduce(callable $callback, $initial = null) {
		return array_reduce($this->items, $callback, $initial);
	}

	/**
	 * Replaces elements from passed arrays into the first array.
	 * @link https://www.php.net/manual/en/function.array-replace.php Description(php.net)
	 * @param  array|ArrayObject|\Traversable $others,... Arrays from which elements will be extracted.
	 * @param  bool              $recursive  (optional) Recursively replace elements.
	 * @return ArrayObject
	 */
	public function replace(...$others) {
		$compareArgs = VariableOptionArgs::create(['bool'])
			->parse(...func_get_args());

		$others = $compareArgs->others;
		$recursive = $compareArgs->option;

		return static::create(
			$recursive
				? $this->replaceRecursive(...$others)
				: array_replace($this->items, ...$others)
		);
	}

	/**
	 * Replaces elements from passed arrays into the first array recursively.
	 * @link https://www.php.net/manual/en/function.array-replace-recursive.php Description(php.net)
	 * @param  array|ArrayObject $others,... Arrays from which elements will be extracted.
	 * @return ArrayObject
	 */
	protected function replaceRecursive(...$others) {
		$result = clone $this;
		$result->recursive();

		foreach ($others as $replacement) {
			foreach ($replacement as $k => $v) {
				$result[$k] = $result->keyExist($k)
					&& static::isArray($v, $result[$k])
						? $result[$k]->replace($v, true)
						: $v;
			}
		}

		return $result;
	}

	/**
	 * Set the internal pointer of an array to its first element.
	 * @link https://www.php.net/manual/en/function.reset.php Description(php.net)
	 */
	public function reset() {
		$this->rewind();
	}

	/**
	 * Return an array with elements in reverse order.
	 * @link https://www.php.net/manual/en/function.array-reverse.php Description(php.net)
	 * @param  boolean $preserveKeys (optional) If set to TRUE numeric keys are preserved.
	 * @return ArrayObject
	 */
	public function reverse($preserveKeys = false) {
		return new static(
			array_reverse($this->items, $preserveKeys)
		);
	}

	/**
	 * Searches the array for a given value and returns the first corresponding key if successful.
	 * @link https://www.php.net/manual/en/function.array-search.php Description(php.net)
	 * @param  mixed   $needle The searched value.
	 * @param  boolean $strict (optional) If set to TRUE the function will use strict(===) comparison.
	 * @return mixed
	 */
	public function search($needle, $strict = false) {
		return array_search($needle, $this->items, $strict);
	}

	/**
	 * Assigns a value to the specified offset
	 * @param  mixed $name  The offset to assign the value to.
	 * @param  mixed $value The value to set.
	 * @return self
	 */
	public function set($name, $value) {
		$this[$name] = $value;

		return $this;
	}

	/**
	 * Shift an element off the beginning of array
	 * @link https://www.php.net/manual/en/function.array-shift.php Description(php.net)
	 * @return mixed
	 */
	public function shift() {
		$keyRemoved = $this->firstKey();
		$valueRemoved = array_shift($this->items);

		$this->getEventManager()->trigger(
			static::EVENT_ITEM_REMOVED,
			$this,
			new ArrayObjectItem($keyRemoved, $valueRemoved)
		);

		return $valueRemoved;
	}

	/**
	 * Extract a slice of the array
	 * @link https://www.php.net/manual/en/function.array-slice.php Description(php.net)
	 * @param  int     $offset       Position where sequence will start.
	 * @param  int     $length       (optional) Length of the sequence.
	 * @param  boolean $preserveKeys (optional) If set to TRUE numeric keys are preserved.
	 * @return ArrayObject
	 */
	public function slice($offset, $length = null, $preserveKeys = false) {
		return new static(
			array_slice($this->items, ...func_get_args())
		);
	}

	/**
	 * Remove a portion of the array and replace it with something else.
	 * @link https://www.php.net/manual/en/function.array-splice.php Description(php.net)
	 * @param  int     $offset       Position where sequence will start.
	 * @param  int     $length       (optional) Length of the sequence.
	 * @param  mixed   $replacement  (optional) An array or value that
	 * will inserted in the place specified by the offset.
	 * @return self
	 */
	public function splice($offset, $length = null, $replacement = []) {
		array_splice($this->items, ...func_get_args());

		$this->getEventManager()->trigger(
			static::EVENT_ITEMS_UPDATED,
			$this
		);

		return $this;
	}

	/**
	 * Calculate the sum of values in an array
	 * @link https://www.php.net/manual/en/function.array-sum.php Description(php.net)
	 * @return number
	 */
	public function sum() {
		return array_sum($this->items);
	}

	/**
	 * Sort an array. Depending on the $flags given can be used on of sort-functions:
	 * [k|a][r]sort. If $callback given will be used one of u[k|a]sort functions.
	 * @link https://www.php.net/manual/en/function.sort.php Description(php.net)
	 * @param  int      $flags    (optional) Sort flags. Can be combined with:
	 * ArrayObject::FLAG_KEY, ArrayObject::FLAG_ASSOC and ArrayObject::SORT_REVERSE.
	 * @param  callable $callback (optional) The comparison function must return
	 * an integer less than, equal to, or greater than zero if the first argument
	 * is considered to be respectively less than, equal to, or greater than the second.
	 * @return self
	 */
	public function sort($flags = SORT_REGULAR, callable $callback = null) {
		$func = '';
		$arg = $flags;

		if ($flags & ArrayObject::FLAG_KEY) {
			$func .= 'k';
		} elseif ($flags & ArrayObject::FLAG_ASSOC) {
			$func .= 'a';
		}

		if ($callback) {
			$func = 'u' . $func;
			$arg = $callback;
		} elseif ($flags & ArrayObject::SORT_REVERSE) {
			$func .= 'r';
		}

		$func .= 'sort';
		$func($this->items, $arg);

		$this->getEventManager()->trigger(
			static::EVENT_SORT,
			$this
		);

		return $this;
	}

	/**
	 * Shuffle an array
	 * @return self
	 */
	public function shuffle() {
		shuffle($this->items);

		$this->getEventManager()->trigger(
			static::EVENT_SORT,
			$this
		);

		return $this;
	}

	/**
	 * Inits offset with $default value if not existed yet.
	 * @param  mixed $key     The offset to init.
	 * @param  mixed $default Default value to set. Callable will be called to retrieve the value.
	 * @return self
	 */
	public function touch($key, $default = null) {
		static::isArray($default)
			&& !is_callable($default)
			&& $default = static::create($default);

		!isset($this[$key])
			&& $this[$key] = is_callable($default)
				? $default($this)
				: $default;

		return $this;
	}

	/**
	 * Unions the arrays
	 * @link https://www.php.net/manual/en/language.operators.array.php Description(php.net)
	 * @param  array|ArrayObject|\Traversable $others,... Other arrays.
	 * @return ArrayObject
	 */
	public function union(...$others) {
		$others = static::create($others, 1);

		ArgumentsTypeChecker::create(
			$others->items,
			...static
				::fill(static::TYPE_ARRAY, $others->count())
				->items
		)->check();

		$result = $this->items(true);
		foreach ($others as $other) {
			$result += $other->items();
		}

		return static::create($result);
	}

	/**
	 * Removes duplicate values from an array
	 * @link https://www.php.net/manual/en/function.array-unique.php Description(php.net)
	 * @param  int $sortFlags (optional) Sort comparison type flag.
	 * @return ArrayObject
	 */
	public function unique($sortFlags = SORT_STRING) {
		return new static(
			array_unique($this->items, $sortFlags)
		);
	}

	/**
	 * Prepend one or more elements to the beginning of an array
	 * @link https://www.php.net/manual/en/function.array-unshift.php Description(php.net)
	 * @param  mixed $values,... The values to prepend.
	 * @return int               Returns the new number of elements in the array
	 */
	public function unshift(...$values) {
		$newCount = array_unshift($this->items, ...$values);

		foreach ($this->slice(0, count($values), true) as $key => $value) {
			$this->getEventManager()->trigger(
				static::EVENT_ITEM_ADDED,
				$this,
				new ArrayObjectItem($key, $value)
			);
		}

		return $newCount;
	}

	/**
	 * Prepend one or more elements to the beginning of an array
	 * @param  mixed $values,... The values to prepend.
	 * @return self
	 */
	public function prepend(...$values) {
		$this->unshift(...$values);

		return $this;
	}

	/**
	 * Return all the values of an array
	 * @link https://www.php.net/manual/en/function.array-values.php Description(php.net)
	 * @return ArrayObject
	 */
	public function values() {
		return new static(
			array_values($this->items)
		);
	}

	/**
	 * Join array elements with a string
	 * @link https://www.php.net/manual/en/function.array-values.php Description(php.net)
	 * @param  string $glue Defaults to an empty string
	 * @return string       Returns a string containing
	 * a string representation of all the array elements in the same order,
	 * with the glue string between each element.
	 */
	public function implode($glue = '') {
		return implode($glue, $this->items);
	}

	/**
	 * Apply a user supplied function to every member of an array.
	 * @link https://www.php.net/manual/en/function.array-walk.php Description(php.net)
	 * @param  callable $callback
	 * @param  bool     $recursive  (optional) Recursively replace elements.
	 * @return self
	 */
	public function walk(callable $callback, $recursive = false) {
		if ($recursive) {
			$this->walkRecursive($callback);
		} else {
			array_walk($this->items, $callback);
		}

		$this->getEventManager()->trigger(
			static::EVENT_ITEMS_UPDATED,
			$this
		);

		return $this;
	}

	/**
	 * Apply a user supplied function recursively to every member of an array.
	 * @link https://www.php.net/manual/en/function.array-walk-recursive.php Description(php.net)
	 * @param  callable $callback
	 */
	protected function walkRecursive(callable $callback) {
		$this->items(true);
		array_walk_recursive($this->items, $callback);
		$this->recursive();
	}

	/**
	 * Returns first value of the array
	 * @return mixed
	 */
	public function first() {
		return $this->items[$this->firstKey()];
	}

	/**
	 * Returns last value of the array
	 * @return mixed
	 */
	public function last() {
		return $this->items[$this->lastKey()];
	}

	/**
	 * Returns first key of the array
	 * @return mixed
	 */
	public function firstKey() {
		return $this->keys()[0];
	}

	/**
	 * Returns last key of the array
	 * @return mixed
	 */
	public function lastKey() {
		$keys = $this->keys();

		return $keys[$keys->count() - 1];
	}
}
