<?php

use ArrayObject\ArrayObject;
use ArrayObject\Exceptions\UndefinedOffsetException;
use ArrayObject\Exceptions\UndefinedPropertyException;
use PHPUnit\Framework\TestCase;

class ArrayObjectInterfacesTest extends TestCase {

	public function testCountable() {
		$a = new ArrayObject([1, 2, 3]);

		$this->assertSame(3, count($a));
		$this->assertSame(3, sizeof($a));

		return $a;
	}

	public function testArrayAccess() {
		$a = new ArrayObject([1, 2, 3]);

		$this->assertSame(1, $a[0]);
		$this->assertSame(2, $a[1]);
		$this->assertSame(3, $a[2]);

		$this->assertFalse(isset($a['v']));
		$a['v'] = 'v';
		$this->assertTrue(isset($a['v']));
		$this->assertSame('v', $a['v']);

		unset($a['asdasd']);
		unset($a['v']);
		$this->assertFalse(isset($a['v']));

		$a[] = 4;
		$this->assertSame(4, $a[3]);

		return $a;
	}

	public function testArrayAccessException() {
		$a = new ArrayObject([1, 2, 3]);

		$this->expectException(UndefinedOffsetException::class);

		$a[3];
	}

	public function testMagic() {
		$a = new ArrayObject([1, 2, 3]);

		$this->assertFalse(isset($a->v));
		$a->v = 'v';
		$this->assertTrue(isset($a->v));
		$this->assertSame('v', $a->v);

		unset($a->v);
		$this->assertFalse(isset($a->v));

		$this->assertSame(
			$a->items(),
			unserialize(serialize($a))->items()
		);

		return $a;
	}

	public function testMagicException() {
		$a = new ArrayObject([1, 2, 3]);

		$this->expectException(UndefinedPropertyException::class);

		$a->v;
	}

	public function testIterator() {
		$a = new ArrayObject([1, 2, 3]);

		$array = [];

		foreach ($a as $k => $v) {
			$array[$k] = $v;
		}

		$this->assertSame([1, 2, 3], $array);
	}

	public function testCurrent() {
		$a = new ArrayObject([1, 2, 3]);

		$this->assertSame(1, $a->current());
		$this->assertSame(10, $a->current(10));

		$a->next();
		$this->assertSame(20, $a->current(20));

		$a->next();
		$this->assertSame(30, $a->current(30));

		$this->assertSame(
			[10, 20, 30],
			$a->items()
		);
	}
}
