<?php

use ArrayObject\ArrayObject;
use ArrayObject\Exceptions\UndefinedOffsetException;
use ArrayObject\Exceptions\UndefinedPropertyException;
use PHPUnit\Framework\TestCase;

ini_set('display_errors', 1);
error_reporting(E_ALL);

class ArrayObjectTest extends TestCase {

	public static function getDefaultValue()
	{
		return 'def';
	}

	private function isPhp7() {
		return phpversion() > 6;
	}

	public function testConstruct() {
		$base = [1, 2, 3];

		$o = ArrayObject::create($base);
		$this->assertSame(
			$base,
			$o->items()
		);

		$generator = $this->generator($base);
		$this->assertTrue($generator instanceof \Traversable);

		$o = ArrayObject::create($generator);
		$this->assertSame(
			$base,
			$o->items()
		);

		$o = ArrayObject::create(new \ArrayIterator($base));
		$this->assertSame(
			$base,
			$o->items()
		);
	}

	public function testConstructRecursive() {
		$o = ArrayObject::create([
			1, 2, 3,
			[
				11, 12, 13,
				[21, 22, 23]
			]
		]);
		$this->assertTrue(is_array(
			$o[3]
		));

		$o->recursive();
		$this->assertTrue(
			$o[3] instanceof ArrayObject
		);
		$this->assertTrue(
			$o[3][3] instanceof ArrayObject
		);

		$this->assertTrue(
			ArrayObject::create(
				[
					1, 2, 3,
					[
						11, 12, 13,
						[21, 22, 23]
					]
				],
				true
			)[3][3] instanceof ArrayObject
		);

		$this->assertTrue(
			is_null(ArrayObject::create([null], true)[0])
		);
	}

	public function testIsArray() {
		$this->assertTrue(ArrayObject::isArray([]));
		$this->assertTrue(
			ArrayObject::isArray(
				ArrayObject::create()
			)
		);
		$this->assertTrue(ArrayObject::isArray(new \ArrayIterator([])));
		$this->assertFalse(ArrayObject::isArray(null));
		$this->assertFalse(ArrayObject::isArray((object)[]));
	}

	public function testEq() {
		$o = new ArrayObject([]);
		$array = [1, 2, 3, 4, 5, 6, $o];

		$a = new ArrayObject($array);

		$this->assertTrue($a->eq($array));
		$this->assertTrue(
			$a->eq(new ArrayObject($array))
		);
		$this->assertTrue(
			$a->eq($this->generator($array))
		);

		$array[6] = new ArrayObject([]);
		$this->assertFalse($a->eq($array));
		$this->assertFalse(
			$a->eq(new ArrayObject($array))
		);
		$this->assertFalse(
			$a->eq($this->generator($array))
		);

		$this->assertFalse($a->eq([1]));
		$this->assertFalse(
			$a->eq(new ArrayObject([1]))
		);
	}

	public function testIsIndexed() {
		$this->assertTrue(
			ArrayObject::create()->isIndexed()
		);
		$this->assertFalse(
			ArrayObject::create()->isAssociative()
		);

		$o = ArrayObject::create(['a', 'b', 'c', 'd']);
		$this->assertTrue($o->isIndexed());
		$this->assertTrue($o->flip()->isAssociative());
	}

	/**
	 * @depends testEq
	 */
	public function testEqException() {
		$this->expectException(\InvalidArgumentException::class);

		ArrayObject::create([1, 2, 3, 4, 5, 6])->eq('string');
	}

	/**
	 * @depends testEqException
	 */
	public function testChunk() {
		$a = new ArrayObject([1, 2, 3, 4, 5, 6]);

		$chunks = $a->chunk(3);

		$this->assertTrue($chunks[0]->eq([1, 2, 3]));
		$this->assertTrue($chunks[1]->eq([4, 5, 6]));

		$chunks = $a->chunk(4, true);

		$this->assertTrue($chunks[0]->eq([1, 2, 3, 4]));
		$this->assertTrue($chunks[1]->eq([
			4 => 5,
			5 => 6,
		]));
	}

	public function testRange() {
		$this->assertSame(
			[1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
			ArrayObject::range(1, 10)
				->items()
		);

		$this->assertSame(
			[0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
			ArrayObject::range(0, 100, 10)
				->items()
		);

		$this->assertSame(
			['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i'],
			ArrayObject::range('a', 'i')
				->items()
		);

		$this->assertSame(
			['c', 'b', 'a'],
			ArrayObject::range('c', 'a')
				->items()
		);
	}

	/**
	 * @depends testEqException
	 */
	public function testChangeKeyCase() {
		$default = [
			'aaa' => 1,
			'BBB' => 2,
			'cCc' => 3,
			'Ddd' => 4,
		];
		$lower = [
			'aaa' => 1,
			'bbb' => 2,
			'ccc' => 3,
			'ddd' => 4,
		];
		$upper = [
			'AAA' => 1,
			'BBB' => 2,
			'CCC' => 3,
			'DDD' => 4,
		];

		$a = new ArrayObject($default);

		$this->assertTrue($a->eq($default));
		$this->assertFalse($a->eq($lower));
		$this->assertFalse($a->eq($upper));

		$this->assertTrue($a->changeKeyCase()->eq($lower));
		$this->assertTrue($a->changeKeyCase(CASE_LOWER)->eq($lower));
		$this->assertTrue($a->changeKeyCase(CASE_UPPER)->eq($upper));

		$this->assertFalse($a->changeKeyCase()->eq($default));
		$this->assertFalse($a->changeKeyCase(CASE_LOWER)->eq($default));
		$this->assertFalse($a->changeKeyCase(CASE_UPPER)->eq($default));
	}

	/**
	 * @depends testEqException
	 */
	public function testColumn() {
		$a = new ArrayObject([
			[
				10,
				11,
				'key' => 'key1',
				'val' => 'val1',
			],
			[
				20,
				21,
				'key' => 'key2',
				'val' => 'val2',
			],
			[
				30,
				31,
				'key' => 'key3',
				'val' => 'val3',
				'foo' => 'foo3',
			],
		]);

		$this->assertSame(
			[
				10, 20, 30
			],
			$a->column(0)->items()
		);
		$this->assertSame(
			[
				11, 21, 31
			],
			$a->column(1)->items()
		);
		$this->assertSame(
			[
				'key1', 'key2', 'key3'
			],
			$a->column('key')->items()
		);

		$this->assertSame(
			[
				'val1' => 10,
				'val2' => 20,
				'val3' => 30,
			],
			$a->column(0, 'val')->items()
		);
		$this->assertSame(
			[
				'key1' => 11,
				'key2' => 21,
				'key3' => 31,
			],
			$a->column(1, 'key')->items()
		);
		$this->assertSame(
			[
				'val1' => 'key1',
				'val2' => 'key2',
				'val3' => 'key3',
			],
			$a->column('key', 'val')->items()
		);

		$this->assertSame(
			['foo3'],
			$a->column('foo')->items()
		);
	}

	public function testCombine() {
		$this->assertSame(
			[
				'a' => 1,
				'b' => 2,
				'c' => 3,
			],
			ArrayObject::combine(['a', 'b', 'c'], [1, 2, 3])->items()
		);

		$this->assertSame(
			[],
			ArrayObject::combine([], [])->items()
		);
	}

	/**
	 * @depends testCombine
	 */
	public function testCombineException() {
		$this->expectException(\InvalidArgumentException::class);

		ArrayObject::combine(1, '2');
	}

	/**
	 * @depends testCombineException
	 */
	public function testCombineWarning() {
		if ($this->isPhp7()) {
			$this->_testCombineWarning7();
		} else {
			$this->_testCombineWarning56();
		}
	}

	private function _testCombineWarning56() {
		$this->expectException(PHPUnit_Framework_Error_Warning::class);

		ArrayObject::combine(['a', 'b', 'c'], [1, 2]);
	}

	private function _testCombineWarning7() {
		$this->expectException(\PHPUnit\Framework\Error\Warning::class);

		ArrayObject::combine(['a', 'b', 'c'], [1, 2]);
	}

	public function testCountValues() {
		$this->assertSame(
			[
				1 => 2,
				'hello' => 2,
				'world' => 1
			],
			ArrayObject::create([1, 'hello', 1, 'world', 'hello'])
				->countValues()
				->items()
		);
	}

	public function testCountValuesWarning() {
		if ($this->isPhp7()) {
			$this->_testCountValuesWarning7();
		} else {
			$this->_testCountValuesWarning56();
		}
	}

	private function _testCountValuesWarning56() {
		$this->expectException(PHPUnit_Framework_Error_Warning::class);

		ArrayObject::create([true, 1])->countValues();
	}

	private function _testCountValuesWarning7() {
		$this->expectException(\PHPUnit\Framework\Error\Warning::class);

		ArrayObject::create([true, 1])->countValues();
	}

	public function testFillWarning() {
		if ($this->isPhp7()) {
			$this->_testFillWarning7();
		} else {
			$this->_testFillWarning56();
		}
	}

	private function _testFillWarning56() {
		$this->expectException(PHPUnit_Framework_Error_Warning::class);
		$this->assertSame(
			[
				'asdad',
				'asdad',
				'asdad',
			],
			ArrayObject::fill('asdad', -1)->items()
		);
	}

	private function _testFillWarning7() {
		$this->expectException(\PHPUnit\Framework\Error\Warning::class);
		$this->assertSame(
			[
				'asdad',
				'asdad',
				'asdad',
			],
			ArrayObject::fill('asdad', -1)->items()
		);
	}

	/**
	 * @depends testFillWarning
	 */
	public function testFill() {
		$this->assertSame(
			[],
			ArrayObject::fill('asdad', 0)->items()
		);
		$this->assertSame(
			[
				'asdad',
				'asdad',
				'asdad',
			],
			ArrayObject::fill('asdad', 3)->items()
		);
		$this->assertSame(
			[
				10 => 'asdad',
				11 => 'asdad',
				12 => 'asdad',
			],
			ArrayObject::fill('asdad', 3, 10)->items()
		);
	}

	/**
	 * @depends testFillWarning
	 */
	public function testFillKeys() {
		$this->assertSame(
			[
				'a' => 'abc',
				'b' => 'abc',
				'c' => 'abc',
			],
			ArrayObject::fill('abc', ['a', 'b', 'c'])->items()
		);
	}

	public function testWalk() {
		$this->assertSame(
			[1, 2, 3],
			ArrayObject::create([1, 2, 3])
				->walk(function($v, $k) {
					$v++;
				})
				->items()
		);
		$this->assertSame(
			[2, 3, 4],
			ArrayObject::create([1, 2, 3])
				->walk(function(&$v, $k) {
					$v++;
				})
				->items()
		);
		$this->assertSame(
			[1, 3, 5],
			ArrayObject::create([1, 2, 3])
				->walk(function(&$v, $k) {
					$v += $k;
				})
				->items()
		);
	}

	public function testWalkRecursive() {
		$o = ArrayObject::create([
			'sweet' => ArrayObject::create([
				'a' => 'apple',
				'b' => 'banana'
			]),
			'sour' => 'lemon'
		]);

		$items = ArrayObject::create();
		$o->walk(
			function($v, $k) use($items) {
				$items[$k] = $v;
			},
			true
		);

		$this->assertSame(
			[
				'a' => 'apple',
				'b' => 'banana',
				'sour' => 'lemon',
			],
			$items->items()
		);
	}

	public function testDiffExceptions() {
		$this->expectException(\InvalidArgumentException::class);

		ArrayObject::create(['a' => 'green', 'red', 'orange', 'blue', 'red'])
			->diff(
				['b' => 'green', 'yellow', 'red'],
				'orange'
			);
	}

	/**
	 * @depends testDiffExceptions
	 */
	public function testDiff() {
		$o = ArrayObject::create(['a' => 'green', 'red', 'blue', 'red']);
		$other = ['b' => 'green', 'yellow', 'red'];

		$this->assertSame(
			[1 => 'blue'],
			$o->diff($other)->items()
		);
		$this->assertSame(
			[1 => 'blue'],
			$o->diff(ArrayObject::create($other))->items()
		);
		$this->assertSame(
			[1 => 'blue'],
			$o->diff($this->generator($other))->items()
		);

		$this->assertSame(
			[2 => 'blue'],
			ArrayObject::create(['a' => 'green', 'red', 'orange', 'blue', 'red'])
				->diff(
					$other,
					['orange']
				)
				->items()
		);
	}

	/**
	 * @depends testDiffExceptions
	 */
	public function testDiffAssoc() {
		$a = ArrayObject::create([
			'a' => 'green',
			'b' => 'brown',
			'c' => 'blue',
			'red'
		]);
		$other = ['a' => 'green', 'yellow', 'red'];

		$this->assertSame(
			[
				'b' => 'brown',
				'c' => 'blue',
				0 => 'red',
			],
			$a->diff($other, ArrayObject::FLAG_ASSOC)
				->items()
		);
		$this->assertSame(
			[
				'b' => 'brown',
				'c' => 'blue',
				0 => 'red',
			],
			$a->diffAssoc($other)->items()
		);
		$this->assertSame(
			[
				'b' => 'brown',
				'c' => 'blue',
				0 => 'red',
			],
			$a->diffAssoc(ArrayObject::create($other))->items()
		);
		$this->assertSame(
			[
				'b' => 'brown',
				'c' => 'blue',
				0 => 'red',
			],
			$a->diffAssoc($this->generator($other))->items()
		);
		$this->assertSame(
			[
				'b' => 'brown',
				'c' => 'blue',
				0 => 'red',
			],
			$a->diffAssoc(ArrayObject::create($other))->items()
		);

		$this->assertSame(
			[
				'c' => 'blue',
				0 => 'red',
			],
			$a->diffAssoc(
				$other,
				['b' => 'brown']
			)
				->items()
		);
	}

	/**
	 * @depends testDiffExceptions
	 */
	public function testDiffKey() {
		$a = ArrayObject::create([
			'blue' => 1,
			'red' => 2,
			'green' => 3,
			'purple' => 4
		]);
		$other = ['green' => 5, 'yellow' => 7, 'cyan' => 8];

		$this->assertSame(
			[
				'blue' => 1,
				'red' => 2,
				'purple' => 4,
			],
			$a->diff($other, ArrayObject::FLAG_KEY)->items()
		);
		$this->assertSame(
			[
				'blue' => 1,
				'red' => 2,
				'purple' => 4,
			],
			$a->diffKey($other)->items()
		);
		$this->assertSame(
			[
				'blue' => 1,
				'red' => 2,
				'purple' => 4,
			],
			$a->diffKey(ArrayObject::create($other))->items()
		);
		$this->assertSame(
			[
				'blue' => 1,
				'red' => 2,
				'purple' => 4,
			],
			$a->diffKey($this->generator($other))->items()
		);

		$this->assertSame(
			[
				'red' => 2,
				'purple' => 4,
			],
			$a->diffKey(
				$other,
				['blue' => 6, 'yellow' => 7, 'mauve' => 8]
			)
				->items()
		);
	}

	/**
	 * @depends testDiffExceptions
	 */
	public function testDiffUser() {
		$a = ArrayObject::create(['a' => 'green', 'b' => 'brown', 'c' => 'blue', 'e' => 'red']);
		$other1 = ['green', 'brown', 'blue', 'red'];
		$other2 = [10 => 'green', 11 => 'brown', 12 => 'blue', 13 => 'red'];

		$this->assertSame(
			['e' =>'red'],
			$a->diff(
				$other1,
				$other2,
				$f1 = function($k1, $v1, $k2, $v2) {
					return $k2 % 10 == ord($k1) - 97 ? 0 : 1;
				}
			)->items()
		);
		$this->assertSame(
			['e' =>'red'],
			$a->diff(
				ArrayObject::create($other1),
				$this->generator($other2),
				$f1
			)->items()
		);
		$this->assertSame(
			['e' =>'red'],
			$a->diff(
				$other1,
				$other2,
				function($k1, $v1, $k2, $v2) {
					return $k2 % 10 == ord($k1) - 97 && $v1 == $v2 ? 0 : 1;
				}
			)->items()
		);
		$this->assertSame(
			[],
			$a->diff(
				$other1,
				$other2,
				function($k1, $v1, $k2, $v2) {
					return $k2 % 10 == ord($k1) - 97 || $v1 == $v2 ? 0 : 1;
				}
			)->items()
		);
		$this->assertSame(
			['a' => 'green', 'b' => 'brown', 'c' => 'blue', 'e' => 'red'],
			$a->diff(
				$other1,
				$other2,
				function($k1, $v1, $k2, $v2) {
					return 1;
				}
			)->items()
		);
	}

	/**
	 * @depends testDiffExceptions
	 */
	public function testDiffRecursive() {
		$a = ArrayObject::create([
			'a' => 'green',
			'b' => 'brown',
			'c' => [
				'd' => 'ddd',
				'e' => 'red',
				's' => [
					[1],
					[2],
				],
			],
			'r' => [1, 2, 3],
		]);

		$this->assertSame(
			[],
			$a->diff($a, ArrayObject::FLAG_RECURSIVE)
				->items()
		);
		$this->assertSame(
			[],
			$a->diffRecursive($a)->items()
		);
		$this->assertSame(
			[],
			$a->diffRecursive(ArrayObject::create($a))->items()
		);

		$other1 = [
			'a' => 'green',
			'b' => 'brooown',
			'c' => [
				'd' => 'ddd',
				'e' => 'red',
				's' => [
					[1],
					[3],
				],
			],
			'r' => [1, 2, 3],
		];
		$other2 = [
			'a' => 'green',
			'b' => 'brown',
			'c' => [
				'd' => 'ddd',
				's' => [
					[1],
					[2],
				],
			],
			'r' => 'rrr',
			'x' => 'xxx',
		];

		$result = [
			'b' => 'brown',
			'c' => [
				'e' => 'red',
				's' => [
					1 => [2],
				],
			],
			'r' => [1, 2, 3],
		];

		$this->assertSame(
			$result,
			$a->diffRecursive($other1, $other2)->items()
		);
		$this->assertSame(
			$result,
			$a
				->diffRecursive(
					ArrayObject::create($other1),
					$this->generator($other2)
				)
				->items()
		);
	}

	public function testFilter() {
		$this->assertSame(
			[
				0 => 'foo',
				2 => -1
			],
			ArrayObject::create([
				0 => 'foo',
				1 => false,
				2 => -1,
				3 => null,
				4 => '',
				5 => '0',
				6 => 0,
			])
				->filter()
				->items()
		);
		$this->assertSame(
			[
				3 => null,
				4 => '',
				5 => '0',
				6 => 0,
			],
			ArrayObject::create([
				0 => 'foo',
				1 => false,
				2 => -1,
				3 => null,
				4 => '',
				5 => '0',
				6 => 0,
			])
				->filter(function($k, $v) {
					return !(in_array($v, ['foo', -1], true) || $k == 1);
				})
				->items()
		);
	}

	public function testFlip() {
		$this->assertSame(
			[
				'oranges' => 0,
				'apples' => 1,
				'pears' => 2,
			],
			ArrayObject::create(['oranges', 'apples', 'pears'])
				->flip()
				->items()
		);

		$this->assertSame(
			[
				1 => 'b',
				2 => 'c',
			],
			ArrayObject::create(['a' => 1, 'b' => 1, 'c' => 2])
				->flip()
				->items()
		);
	}

	public function testTouch() {
		$a = ArrayObject::create([]);

		$this->assertSame([], $a->items());
		$this->assertSame(3, $a->touch(1, 3)[1]);
		$this->assertSame([1 => 3], $a->items());
		$this->assertSame(3, $a->touch(1, 5)[1]);
		$this->assertSame(null, $a->touch(2)[2]);
		$this->assertSame([1 => 3, 2 => null], $a->items());
		$this->assertSame(
			[1],
			$a->touch(3, [])[3]->append(1)->items()
		);
		$this->assertSame(
			[1 => 3, 2 => null, 3 => [1]],
			$a->items(true)
		);
		$this->assertSame(
			$a,
			$a->touch(4, function ($a) {return $a;})[4]
		);
		$this->assertSame(
			[1 => 3, 2 => null, 3 => [1], 4 => $a, 5 => 'def'],
			$a->touch(5, [static::class, 'getDefaultValue'])->items()
		);
	}

	public function testHas() {
		$a = ArrayObject::create([
			'adssa',
			'zxc',
			'qweqwe',
			4,
			5,
			7,
			2,
		]);

		$this->assertTrue($a->has('zxc'));
		$this->assertFalse($a->has('jkl'));
	}

	public function testIntersectExceptions() {
		$this->expectException(\InvalidArgumentException::class);

		ArrayObject::create(['a' => 'green', 'red', 'orange', 'blue', 'red'])
			->intersect(
				['b' => 'green', 'yellow', 'red'],
				'orange'
			);
	}

	/**
	 * @depends testIntersectExceptions
	 */
	public function testIntersect() {
		$a = ArrayObject::create(['a' => 'green', 'red', 'blue']);
		$other1 = ['b' => 'green', 'yellow', 'red'];
		$other2 = ['b' => 'green', 'yellow'];

		$this->assertSame(
			['a' => 'green', 0 => 'red'],
			$a->intersect($other1)->items()
		);
		$this->assertSame(
			['a' => 'green'],
			$a->intersect($other1, $other2)->items()
		);
		$this->assertSame(
			['a' => 'green'],
			$a
				->intersect(
					ArrayObject::create($other1),
					$this->generator($other2)
				)
				->items()
		);
	}

	/**
	 * @depends testIntersectExceptions
	 */
	public function testIntersectAssoc() {
		$a = ArrayObject::create(['a' => 'green', 'b' => 'brown', 'c' => 'blue', 'red']);
		$other = ['a' => 'green', 'b' => 'yellow', 'blue', 'red'];

		$this->assertSame(
			['a' => 'green'],
			$a->intersect($other, ArrayObject::FLAG_ASSOC)->items()
		);
		$this->assertSame(
			['a' => 'green'],
			$a->intersectAssoc(ArrayObject::create($other))->items()
		);
		$this->assertSame(
			['a' => 'green'],
			$a->intersectAssoc($this->generator($other))->items()
		);
	}

	/**
	 * @depends testIntersectExceptions
	 */
	public function testIntersectKey() {
		$a = ArrayObject::create(['blue' => 1, 'red' => 2, 'green' => 3, 'purple' => 4]);
		$other = ['green' => 5, 'blue' => 6, 'yellow' => 7, 'cyan'   => 8];

		$this->assertSame(
			['blue' => 1, 'green' => 3],
			$a->intersect($other, ArrayObject::FLAG_KEY)->items()
		);
		$this->assertSame(
			['blue' => 1, 'green' => 3],
			$a->intersectKey(ArrayObject::create($other))->items()
		);
		$this->assertSame(
			['blue' => 1, 'green' => 3],
			$a->intersectKey($this->generator($other))->items()
		);
	}

	/**
	 * @depends testIntersectExceptions
	 */
	public function testIntersectUser() {
		$a = ArrayObject::create([
			'a' => 'green',
			'b' => 'brown',
			'c' => 'blue',
			'e' => 'red'
		]);
		$other1 = ['green', 'brown', 'blue', 'red'];
		$other2 = [10 => 'green', 11 => 'brown', 12 => 'blue', 13 => 'red'];

		$this->assertSame(
			['a' => 'green', 'b' => 'brown', 'c' => 'blue'],
			$a
				->intersect(
					$other1,
					$other2,
					function($k1, $v1, $k2, $v2) {
						return $k2 % 10 == ord($k1) - 97 ? 0 : 1;
					}
				)
				->items()
		);
		$this->assertSame(
			['a' => 'green', 'b' => 'brown', 'c' => 'blue'],
			$a
				->intersect(
					$other1,
					$other2,
					function($k1, $v1, $k2, $v2) {
						return $k2 % 10 == ord($k1) - 97 && $v1 == $v2 ? 0 : 1;
					}
				)
				->items()
		);
		$this->assertSame(
			['a' => 'green', 'b' => 'brown', 'c' => 'blue', 'e' => 'red'],
			$a
				->intersect(
					$other1,
					$other2,
					function($k1, $v1, $k2, $v2) {
						return $k2 % 10 == ord($k1) - 97 || $v1 == $v2 ? 0 : 1;
					}
				)
				->items()
		);
		$this->assertSame(
			['a' => 'green', 'b' => 'brown', 'c' => 'blue', 'e' => 'red'],
			$a
				->intersect(
					ArrayObject::create($other1),
					$this->generator($other2),
					function($k1, $v1, $k2, $v2) {
						return $k2 % 10 == ord($k1) - 97 || $v1 == $v2 ? 0 : 1;
					}
				)
				->items()
		);
		$this->assertSame(
			[],
			$a
				->intersect(
					$other1,
					$other2,
					function($k1, $v1, $k2, $v2) {
						return 1;
					}
				)
				->items()
		);
	}

	public function testKeys() {
		$this->assertSame(
			['a', 'b', 'c', 'e'],
			ArrayObject::create(['a' => 'green', 'b' => 'brown', 'c' => 'blue', 'e' => 'red'])
				->keys()
				->items()
		);

		$this->assertSame(
			['a', 'c'],
			ArrayObject::create(['a' => 1, 'b' => 'brown', 'c' => '1', 'e' => 'red'])
				->keys(1)
				->items()
		);

		$this->assertSame(
			['a'],
			ArrayObject::create(['a' => 1, 'b' => 'brown', 'c' => '1', 'e' => 'red'])
				->keys(1, true)
				->items()
		);
	}

	public function testKeyExist() {
		$a = ArrayObject::create([
			'a' => 'green',
			'b' => 'brown',
			'c' => 'blue',
			'd' => null,
			'red'
		]);
		$this->assertTrue($a->keyExist('a'));
		$this->assertTrue($a->keyExist(0));
		$this->assertFalse($a->keyExist(1));
		$this->assertTrue($a->keyExist('d'));
		$this->assertFalse(isset($a['d']));
	}

	public function testMergeException() {
		$this->expectException(\InvalidArgumentException::class);

		ArrayObject::create(['color' => 'red', 2, 4])
			->merge(false);
	}

	/**
	 * @depends testMergeException
	 */
	public function testMerge() {
		$a = ArrayObject::create(['color' => 'red', 2, 4]);
		$other = ['a', 'b', 'color' => 'green', 'shape' => 'trapezoid', 4];

		$this->assertSame(
			$result = [
				'color' => 'green',
				0 => 2,
				1 => 4,
				2 => 'a',
				3 => 'b',
				'shape' => 'trapezoid',
				4 => 4,
			],
			$a->merge($other)->items()
		);
		$this->assertSame(
			$result,
			$a->merge(ArrayObject::create($other))->items()
		);
		$this->assertSame(
			$result,
			$a->merge($this->generator($other))->items()
		);
		$this->assertSame(
			$a->items(),
			$a->merge()->items()
		);
	}

	/**
	 * @depends testMergeException
	 */
	public function testMergeRecursive() {
		$a = ArrayObject::create(['color' => ['favorite' => 'red'], 5]);
		$other1 = [10, 'color' => ['favorite' => 'green', 'blue']];
		$other2 = ['color' => ['favorite' => 'yellow']];

		$this->assertSame(
			[
				'color' => [
					'favorite' => [
						0 => 'red',
						1 => 'green',
					],
					0 => 'blue',
				],
				0 => 5,
				1 => 10,
			],
			$a->merge($other1, true)->items()
		);

		$this->assertSame(
			[
				'color' => [
					'favorite' => [
						0 => 'red',
						1 => 'green',
						2 => 'yellow',
					],
					0 => 'blue',
				],
				0 => 5,
				1 => 10,
			],
			$a
				->merge(
					ArrayObject::create($other1),
					$this->generator($other2),
					true
				)
				->items()
		);
	}

	public function testMapException() {
		$this->expectException(\InvalidArgumentException::class);

		ArrayObject::create([])->map(
			function() {},
			ArrayObject::create([]),
			1
		);
	}

	/**
	 * @depends testMapException
	 */
	public function testMap() {
		$a = ArrayObject::create(range(1, 5));

		$this->assertSame(
			[1, 8, 27, 64, 125],
			$a->map(function($v) {return $v * $v * $v;})->items()
		);

		$this->assertSame(
			$result = [
				'The number 1 is called uno in Spanish',
				'The number 2 is called dos in Spanish',
				'The number 3 is called tres in Spanish',
				'The number 4 is called cuatro in Spanish',
				'The number 5 is called cinco in Spanish',
			],
			$a
				->map(
					$f = function($n, $m) {
						return "The number {$n} is called {$m} in Spanish";
					},
					$other = ['uno', 'dos', 'tres', 'cuatro', 'cinco']
				)
				->items()
		);

		$this->assertSame(
			$result,
			$a->map($f, ArrayObject::create($other))->items()
		);
		$this->assertSame(
			$result,
			$a->map($f, $this->generator($other))->items()
		);
	}

	public function testMultisort() {
		$a1 = [10, 100, 100, 100, 0];
		$ao1 = ArrayObject::create($a1);
		$a2 = [1,    3,   2,   2, 4];
		$ao2 = ArrayObject::create($a2);
		$a3 = [0,    0,   9,   8, 0];
		$ao3 = ArrayObject::create($a3);

		$this->assertSame(
			[
				[0, 10, 100, 100, 100],
				[4, 1, 2, 2, 3]
			],
			[
				$ao1->multisort($ao2)
					->items(),
				$ao2->items()
			]
		);

		$this->assertSame(
			[
				[0, 10, 100, 100, 100],
				[4, 1, 2, 2, 3],
				[0, 0, 9, 8, 0]
			],
			[
				$ao1->multisort($ao2, $ao3, SORT_DESC)
					->items(),
				$ao2->items(),
				$ao3->items()
			]
		);

		$ao4 = ArrayObject::create([
			ArrayObject::create(['10', 11, 100, 100, 'a']),
			ArrayObject::create([   1,  2, '2',   3,   1])
		]);

		$this->assertSame(
			[
				['10', 100, 100, 11, 'a'],
				[   1,   3, '2',   2,   1]
			],
			[
				$ao4[0]->multisort(
					SORT_ASC, SORT_STRING,
					$ao4[1], SORT_NUMERIC, SORT_DESC
				)->items(),
				$ao4[1]->items()
			]
		);

		if ($this->isPhp7()) {
			$this->_testMultisort7();
		} else {
			$this->_testMultisort56();
		}
	}

	private function _testMultisort56() {
		$this->assertSame(
			['1', 9, '10', 'a', 10],
			ArrayObject::create(['1', 10, '10', 9, 'a'])
				->multisort()
				->items()
		);
	}

	private function _testMultisort7() {
		$this->assertSame(
			['1', 9, 10, '10', 'a'],
			ArrayObject::create(['1', 10, '10', 9, 'a'])
				->multisort()
				->items()
		);
	}

	public function testPad() {
		$this->assertSame(
			[12, 10, 9, 0, 0],
			ArrayObject::create([12, 10, 9])
				->pad(5, 0)
				->items()
		);

		$this->assertSame(
			[-1, -1, -1, -1, 12, 10, 9],
			ArrayObject::create([12, 10, 9])
				->pad(-7, -1)
				->items()
		);

		$this->assertSame(
			[12, 10, 9],
			ArrayObject::create([12, 10, 9])
				->pad(2, 'asdsd')
				->items()
		);
	}

	public function testProduct() {
		$this->assertSame(
			24,
			ArrayObject::create([1, 2, 3, 4])->product()
		);

		$this->assertSame(
			1,
			ArrayObject::create()->product()
		);

		$this->assertSame(
			0,
			ArrayObject::create([1, 2, 'asdasd', 3, 4])->product()
		);

		$this->assertSame(
			25.2,
			ArrayObject::create([1, 2.1, 3, 4])->product()
		);
	}

	public function testPop() {
		$a = ArrayObject::create(['a' => 'green', 'b' => 'brown', 'c' => 'blue', 'e' => 'red']);

		$this->assertSame(
			'red',
			$a->pop()
		);
		$this->assertSame(
			'blue',
			$a->pop()
		);
	}

	public function testPush() {
		$a = ArrayObject::create(['a' => 'green', 'b' => 'brown', 'c' => 'blue', 'e' => 'red']);

		$this->assertSame(
			5,
			$a->push('apple')
		);
		$this->assertSame(
			8,
			$a->push(1, 2, 3)
		);
		$this->assertSame(
			['a' => 'green', 'b' => 'brown', 'c' => 'blue', 'e' => 'red', 'apple', 1, 2, 3],
			$a->items()
		);
	}

	public function testAppend() {
		$a = ArrayObject::create(['a' => 'green', 'b' => 'brown', 'c' => 'blue', 'e' => 'red']);

		$this->assertSame(
			$a,
			$a->append('apple')
		);
		$this->assertSame(
			5,
			$a->count()
		);
		$this->assertSame(
			$a,
			$a->append(1, 2, 3)
		);
		$this->assertSame(
			8,
			$a->count()
		);
		$this->assertSame(
			['a' => 'green', 'b' => 'brown', 'c' => 'blue', 'e' => 'red', 'apple', 1, 2, 3],
			$a->items()
		);
	}

	public function testShift() {
		$a = ArrayObject::create(['a' => 'green', 'b' => 'brown', 'c' => 'blue', 'e' => 'red']);

		$this->assertSame(
			'green',
			$a->shift()
		);
		$this->assertSame(
			'brown',
			$a->shift()
		);
	}

	public function testUnshift() {
		$a = ArrayObject::create(['a' => 'green', 'b' => 'brown', 'c' => 'blue', 'e' => 'red']);

		$this->assertSame(
			5,
			$a->unshift('apple')
		);
		$this->assertSame(
			8,
			$a->unshift(1, 2, 3)
		);
		$this->assertSame(
			[1, 2, 3, 'apple', 'a' => 'green', 'b' => 'brown', 'c' => 'blue', 'e' => 'red'],
			$a->items()
		);
	}

	public function testFirstKey() {
		$a = ArrayObject::create(['a' => 'green', 'b' => 'brown', 'c' => 'blue', 'e' => 'red']);

		$this->assertSame(
			'a',
			$a->firstKey()
		);
		$this->assertSame(
			'a',
			$a->firstKey()
		);
	}

	public function testLastKey() {
		$a = ArrayObject::create(['a' => 'green', 'b' => 'brown', 'c' => 'blue', 'e' => 'red']);

		$this->assertSame(
			'e',
			$a->lastKey()
		);
		$this->assertSame(
			'e',
			$a->lastKey()
		);
	}

	public function testFirst() {
		$a = ArrayObject::create(['a' => 'green', 'b' => 'brown', 'c' => 'blue', 'e' => 'red']);

		$this->assertSame(
			'green',
			$a->first()
		);
		$this->assertSame(
			'green',
			$a->first()
		);
	}

	public function testLast() {
		$a = ArrayObject::create(['a' => 'green', 'b' => 'brown', 'c' => 'blue', 'e' => 'red']);

		$this->assertSame(
			'red',
			$a->last()
		);
		$this->assertSame(
			'red',
			$a->last()
		);
	}

	public function testRandKeys() {
		$o = ArrayObject::create([
			'Neo', 'Morpheus', 'Trinity', 'Cypher', 'Tank'
		]);

		$r = $o->randKeys();

		$this->assertSame(1, $r->count());
		$this->assertTrue(isset($o[$r->first()]));

		$r = $o->randKeys(2);

		$this->assertSame(2, $r->count());
		$this->assertTrue(isset($o[$r[0]]));
		$this->assertTrue(isset($o[$r[1]]));
	}

	/**
	 * @depends testIntersectAssoc
	 */
	public function testRand() {
		$o = ArrayObject::create([
			'a' => 'Neo',
			'b' => 'Morpheus',
			'c' => 'Trinity',
			'd' => 'Cypher',
			'e' => 'Tank'
		]);

		$r = $o->rand();
		$this->assertSame(1, $r->count());
		$this->assertSame(
			$r->items(),
			$o->intersect($r, ArrayObject::FLAG_ASSOC)
				->items()
		);

		$r = $o->rand(3);
		$this->assertSame(3, $r->count());
		$this->assertSame(
			$r->items(),
			$o->intersect($r, ArrayObject::FLAG_ASSOC)
				->items()
		);
	}

	public function testReduce() {
		function sum($carry, $item) {
			$carry += $item;
			return $carry;
		}

		function product($carry, $item) {
			$carry *= $item;
			return $carry;
		}

		$this->assertSame(
			15,
			ArrayObject::create([1, 2, 3, 4, 5])
				->reduce('sum')
		);

		$this->assertSame(
			1200,
			ArrayObject::create([1, 2, 3, 4, 5])
				->reduce('product', 10)
		);

		$initial = 'No data to reduce';
		$this->assertSame(
			$initial,
			ArrayObject::create()
				->reduce('sum', $initial)
		);
	}

	public function testReplaceException() {
		$this->expectException(\InvalidArgumentException::class);

		ArrayObject::create([1, 2, 3, 4])
			->replace([], 1);
	}

	/**
	 * @depends testReplaceException
	 */
	public function testReplace() {
		$this->assertSame(
			[11, 12, 3, [101, 102, 103]],
			ArrayObject::create([1, 2, 3, [101, 102, 103]])
				->replace([11, 12])
				->items()
		);

		$this->assertSame(
			[21, 12, 3, [201]],
			ArrayObject::create([1, 2, 3, [101, 102, 103]])
				->replace(
					[11, 12],
					ArrayObject::create([21]),
					$this->generator([3 => [201]])
				)
				->items()
		);
	}

	/**
	 * @depends testReplaceException
	 */
	public function testReplaceRecursive() {
		$this->assertSame(
			[21, 12, 3, [201, 102, 103]],
			ArrayObject::create([1, 2, 3, [101, 102, 103]])
				->replace(
					[11, 12],
					ArrayObject::create([21, 3 => [201]]),
					true
				)
				->items(true)
		);

		$a = ArrayObject::create([
			0,
			1,
			'a' => ['b' => [0, 'c' => null]],
			'b' => ['b' => [0, 'c' => null]],
			'c' => ['b' => [0, 'c' => null]],
		]);
		$other1 = [1, 2 => 2, 'a' => ['b' => ['c' => ['d' => null]]]];
		$other2 = [1, 2 => 2, 'b' => ['b' => null]];
		$other3 = [1, 2 => 2, 'c' => ['b' => ['c' => 1]]];

		$this->assertSame(
			[
				1,
				1,
				'a' => ['b' => [0, 'c' => ['d' => null]]],
				'b' => ['b' => null],
				'c' => ['b' => [0, 'c' => 1]],
				2
			],
			$a
				->replace(
					$other1,
					ArrayObject::create($other2),
					$this->generator($other3),
					true
				)
				->items(true)
		);
	}

	public function testReset() {
		$a = ArrayObject::create([1, 2, 3, 4, 5]);

		$this->assertSame(1, $a->current());
		$this->assertSame(2, $a->next());
		$this->assertSame(3, $a->next());
		$this->assertSame(3, $a->current());
		$a->reset();
		$this->assertSame(1, $a->current());
	}

	public function testReverse() {
		$this->assertSame(
			[
				6,
				5,
				4,
				'c' => 3,
				'b' => 2,
				'a' => 1,
			],
			ArrayObject::create([
				'a' => 1,
				'b' => 2,
				'c' => 3,
				4,
				5,
				6
			])
				->reverse()
				->items()
		);

		$this->assertSame(
			[
				2 => 6,
				1 => 5,
				0 => 4,
				'c' => 3,
				'b' => 2,
				'a' => 1,
			],
			ArrayObject::create([
				'a' => 1,
				'b' => 2,
				'c' => 3,
				4,
				5,
				6
			])
				->reverse(true)
				->items()
		);

		$this->assertSame(
			[
				2 => 0,
				1 => 1,
				0 => 2,
			],
			ArrayObject::create([
				0 => 0,
				1 => 1,
				2 => 2,
			])
				->reverse()
				->reverse(true)
				->items()
		);
	}

	public function testSearch() {
		$o = ArrayObject::create([
			0 => 'blue',
			1 => 'red',
			2 => 'green',
			3 => 'red',
		]);

		$this->assertSame(
			2,
			$o->search('green')
		);

		$this->assertSame(
			1,
			$o->search('red')
		);
	}

	public function testSet() {
		$o = ArrayObject::create()
			->set('a', 1)
			->set('b', 2)
			->set('a', 5)
			->set('c', 3);

		$this->assertSame(
			['a' => 5, 'b' => 2, 'c' => 3],
			$o->items()
		);
	}

	public function testSlice() {
		$o = ArrayObject::create(['a', 'b', 'c', 'd', 'e']);

		$this->assertSame(
			['c', 'd', 'e'],
			$o->slice(2)
				->items()
		);

		$this->assertSame(
			['d'],
			$o->slice(-2, 1)
				->items()
		);

		$this->assertSame(
			['a', 'b', 'c'],
			$o->slice(0, 3)
				->items()
		);

		$this->assertSame(
			['c', 'd'],
			$o->slice(2, -1)
				->items()
		);

		$this->assertSame(
			[2 => 'c', 3 => 'd'],
			$o->slice(2, -1, true)
				->items()
		);
	}

	public function testSplice() {
		$this->assertSame(
			['red', 'green'],
			ArrayObject::create(['red', 'green', 'blue', 'yellow'])
				->splice(2)
				->items()
		);

		$this->assertSame(
			['red', 'yellow'],
			ArrayObject::create(['red', 'green', 'blue', 'yellow'])
				->splice(1, -1)
				->items()
		);

		$o = ArrayObject::create(['red', 'green', 'blue', 'yellow']);
		$this->assertSame(
			['red', 'orange'],
			$o
				->splice(1, $o->count(), 'orange')
				->items()
		);

		$this->assertSame(
			['red', 'green', 'blue', 'black', 'maroon'],
			ArrayObject::create(['red', 'green', 'blue', 'yellow'])
				->splice(-1, 1, ['black', 'maroon'])
				->items()
		);
	}

	public function testSum() {
		$this->assertSame(
			20,
			ArrayObject::create([2, '4', 6, 8])
				->sum()
		);

		$this->assertSame(
			6.9,
			ArrayObject::create(['a' => 1.2, 'b' => 2.3, 'c' => 3.4])
				->sum()
		);
	}

	public function testSort() {
		$this->assertSame(
			[
				'apple',
				'banana',
				'lemon',
				'orange',
			],
			ArrayObject::create(['lemon', 'orange', 'banana', 'apple'])
				->sort()
				->items()
		);

		$this->assertSame(
			[
				'Orange1',
				'orange2',
				'Orange3',
				'orange20',
			],
			ArrayObject::create([
				'Orange1',
				'orange2',
				'orange20',
				'Orange3',
			])
				->sort(SORT_NATURAL | SORT_FLAG_CASE)
				->items()
		);
	}

	public function testRSort() {
		$this->assertSame(
			[
				'orange',
				'lemon',
				'banana',
				'apple',
			],
			ArrayObject::create(['lemon', 'orange', 'banana', 'apple'])
				->sort(ArrayObject::SORT_REVERSE)
				->items()
		);
	}

	public function testKSort() {
		$this->assertSame(
			[
				'a'=>'orange',
				'b'=>'banana',
				'c'=>'apple',
				'd'=>'lemon',
			],
			ArrayObject::create([
				'd'=>'lemon',
				'a'=>'orange',
				'b'=>'banana',
				'c'=>'apple',
			])
				->sort(ArrayObject::FLAG_KEY)
				->items()
		);
	}

	public function testKRSort() {
		$this->assertSame(
			[
				'd'=>'lemon',
				'c'=>'apple',
				'b'=>'banana',
				'a'=>'orange',
			],
			ArrayObject::create([
				'd'=>'lemon',
				'a'=>'orange',
				'b'=>'banana',
				'c'=>'apple',
			])
				->sort(ArrayObject::FLAG_KEY | ArrayObject::SORT_REVERSE)
				->items()
		);
	}

	public function testASort() {
		$this->assertSame(
			[
				'c'=>'apple',
				'b'=>'banana',
				'd'=>'lemon',
				'a'=>'orange',
			],
			ArrayObject::create([
				'd'=>'lemon',
				'a'=>'orange',
				'b'=>'banana',
				'c'=>'apple',
			])
				->sort(ArrayObject::FLAG_ASSOC)
				->items()
		);
	}

	public function testARSort() {
		$this->assertSame(
			[
				'a'=>'orange',
				'd'=>'lemon',
				'b'=>'banana',
				'c'=>'apple',
			],
			ArrayObject::create([
				'd'=>'lemon',
				'a'=>'orange',
				'b'=>'banana',
				'c'=>'apple',
			])
				->sort(ArrayObject::FLAG_ASSOC | ArrayObject::SORT_REVERSE)
				->items()
		);
	}

	public function testUSort() {
		$this->assertSame(
			[1, 2, 3, 5, 6],
			ArrayObject::create([3, 2, 5, 6, 1])
				->sort(null, function($a, $b) {
					if ($a == $b) {
						return 0;
					}

					return ($a < $b) ? -1 : 1;
				})
				->items()
		);

		$this->assertSame(
			[
				'apples',
				'grapes',
				'lemons',
			],
			ArrayObject::create([
				'lemons',
				'apples',
				'grapes',
			])
				->sort(null, function($a, $b) {
					return strcmp($a, $b);
				})
				->items()
		);
	}

	public function testUKSort() {
		$this->assertSame(
			[
				'an apple' => 3,
				'a banana' => 4,
				'the Earth' => 2,
				'John' => 1,
			],
			ArrayObject::create([
				'John' => 1,
				'the Earth' => 2,
				'an apple' => 3,
				'a banana' => 4,
			])
				->sort(ArrayObject::FLAG_KEY, function($a, $b) {
					$a = preg_replace('@^(a|an|the) @', '', $a);
					$b = preg_replace('@^(a|an|the) @', '', $b);

					return strcasecmp($a, $b);
				})
				->items()
		);
	}

	public function testUASort() {
		$this->assertSame(
			[
				'd' => -9,
				'h' => -4,
				'c' => -1,
				'e' => 2,
				'g' => 3,
				'a' => 4,
				'f' => 5,
				'b' => 8,
			],
			ArrayObject::create([
				'a' => 4,
				'b' => 8,
				'c' => -1,
				'd' => -9,
				'e' => 2,
				'f' => 5,
				'g' => 3,
				'h' => -4,
			])
				->sort(ArrayObject::FLAG_ASSOC, function($a, $b) {
					if ($a == $b) {
						return 0;
					}

					return ($a < $b) ? -1 : 1;
				})
				->items()
		);
	}

	/**
	 * @depends testHas
	 */
	public function testShuffle() {
		$a = ArrayObject::create([1, 2, 3, 4, 5]);

		$this->assertTrue(
			[1, 2, 3, 4, 5] == $a->items()
		);

		$a->shuffle();

		$this->assertFalse(
			[1, 2, 3, 4, 5] == $a->items()
		);

		$this->assertTrue($a->has(1));
		$this->assertTrue($a->has(2));
		$this->assertTrue($a->has(3));
		$this->assertTrue($a->has(4));
		$this->assertTrue($a->has(5));

		$this->assertFalse($a->has(6));
	}

	public function testUnion() {
		$this->assertSame(
			[1, 1, 2, 2, 3, 3],
			ArrayObject
				::fill(1, 2)
				->union(
					ArrayObject::fill(2, 3, 1),
					ArrayObject::fill(3, 3, 3)->items()
				)
				->items()
		);
	}

	public function testUnique() {
		$this->assertSame(
			['a' => 'green', 'red', 'blue'],
			ArrayObject::create(['a' => 'green', 'red', 'b' => 'green', 'blue', 'red'])
				->unique()
				->items()
		);

		$this->assertSame(
			[4, 2 => '3'],
			ArrayObject::create([4, '4', '3', 4, 3, '3'])
				->unique()
				->items()
		);
	}

	public function testValues() {
		$this->assertSame(
			['XL', 'gold'],
			ArrayObject::create(['size' => 'XL', 'color' => 'gold'])
				->values()
				->items()
		);
	}

	public function testImplode() {
		$a = ArrayObject::create([1, 2, 3]);

		$this->assertSame(
			'123',
			$a->implode()
		);
		$this->assertSame(
			'1, 2, 3',
			$a->implode(', ')
		);
	}

	public function testGenerator() {
		$a = ArrayObject::create(range(0, 2));

		$this->assertSame(
			[1, 2, 3],
			$a
				->generate(function ($key, $value) {
					yield $value + 1;
				})
				->items()
		);

		$chA = 65;
		$this->assertSame(
			['A' => 1, 'B' => 2, 'C' => 3],
			$a
				->generate(function ($key, $value) use ($chA) {
					yield chr($chA + $key) => $value + 1;
				})
				->items()
		);
		$this->assertSame(
			['A' => 1, 'B' => 2, 'C' => 2, 'D' => 3, 'E' => 3, 'F' => 4],
			$a
				->generate(function ($key, $value) use ($chA) {
					yield chr($chA + $key * 2) => $value + 1;
					yield chr($chA + $key * 2 + 1) => $value + 2;
				})
				->items()
		);

		$this->assertSame(
			['A' => 2, 'B' => 3, 'I' => 4],
			$a
				->generate(
					function (ArrayObject $keys, ArrayObject $values) use ($chA) {
						yield chr($chA + $keys->product()) => $values->sum();
					},
					[ArrayObject::fill(1, 3), array_fill(0, 3, 1)]
				)
				->items()
		);
		$this->assertSame(
			[3, 3],
			$a
				->generate(
					function (ArrayObject $keys, ArrayObject $values) {
						yield 1 * $keys->product() => 1 * $values->sum();
					},
					[ArrayObject::fill(1, 3), array_fill(0, 2, 1)],
					MultipleIterator::MIT_NEED_ANY
				)
				->items()
		);

		ArrayObject::create([1])
			->generate(
				function ($keys, $values) {
					$this->assertSame(
						['this' => 0, 'other' => 1],
						$keys->items()
					);
					$this->assertSame(
						['this' => 1, 'other' => 2],
						$values->items()
					);

					yield;
				},
				['other' => [1 => 2]],
				MultipleIterator::MIT_KEYS_ASSOC
			);

		$this->assertSame(
			[],
			ArrayObject::create([])
				->generate(function ($k, $v) {
					yield $v;
				})
				->items()
		);

		$this->assertSame(
			[1, 2],
			ArrayObject::create([0, 1, 2])
				->generate(function ($k, $v) {
					$v && (yield $v);
				})
				->items()
		);
	}

	private function generator($items)
	{
		foreach ($items as $k => $item) {
			yield $k => $item;
		}
	}
}
