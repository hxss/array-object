<?php

use ArrayObject\ArrayObject;
use ArrayObject\Contrib\ArrayObjectItem;
use ArrayObject\Exceptions\UndefinedOffsetException;
use ArrayObject\Exceptions\UndefinedPropertyException;
use PHPUnit\Framework\TestCase;

class ArrayObjectEventsTest extends TestCase {

	private function isPhp7() {
		return phpversion() > 6;
	}

	public function testItemsUpdated() {
		$o = ArrayObject::create([1, 2, [3, 4, 5]]);

		$countEvents = 0;
		$o->getEventManager()->attach(ArrayObject::EVENT_ITEMS_UPDATED, function($e) use(&$countEvents, $o) {
			$this->assertTrue($e->getTarget() === $o);
			$countEvents++;
		});

		$o->recursive();
		$o->walk(function($v) {});

		$this->assertSame(2, $countEvents);

		$countEvents = 0;
		$o->splice(1);
		$this->assertSame(1, $countEvents);
	}

	public function testItemAdded() {
		$o = ArrayObject::create([1, 2, [3, 4, 5]]);

		$countEvents = 0;
		$expectedItem = new ArrayObjectItem(null, null);
		$listener = function($e) use(&$countEvents, $o, $expectedItem) {
			$this->assertTrue($e->getTarget() === $o);
			$this->assertSame($expectedItem->key, $e->getParams()->key);
			$this->assertSame($expectedItem->value, $e->getParams()->value);

			$countEvents++;
		};
		$o->getEventManager()->attach(ArrayObject::EVENT_ITEM_ADDED, $listener);

		$expectedItem->key = 'a';
		$expectedItem->value = 'aaa';
		$o['a'] = 'aaa';

		$expectedItem->key = 3;
		$expectedItem->value = 'push';
		$o->push('push');

		$expectedItem->key = 0;
		$expectedItem->value = 'unshift';
		$o->unshift('unshift');

		$o->getEventManager()->detach($listener);
		$listener = function($e) use(&$countEvents, $o, $expectedItem) {
			$this->assertTrue($e->getTarget() === $o);
			$countEvents++;
		};
		$o->getEventManager()->attach(ArrayObject::EVENT_ITEM_ADDED, $listener);

		$o->push('push1', 'push2');
		$o->unshift('unshift1', 'unshift2');

		$this->assertSame(7, $countEvents);
	}

	public function testItemRemoved() {
		$o = ArrayObject::create([1, 2, [3, 4, 5], 6, 7]);

		$countEvents = 0;
		$expectedItem = new ArrayObjectItem(null, null);
		$listener = function($e) use(&$countEvents, $o, $expectedItem) {
			$this->assertTrue($e->getTarget() === $o);
			$this->assertSame($expectedItem->key, $e->getParams()->key);
			$this->assertSame($expectedItem->value, $e->getParams()->value);

			$countEvents++;
		};
		$o->getEventManager()->attach(ArrayObject::EVENT_ITEM_REMOVED, $listener);

		$expectedItem->key = 2;
		$expectedItem->value = [3, 4, 5];
		unset($o[2]);

		$expectedItem->key = 0;
		$expectedItem->value = 1;
		$o->shift();

		$expectedItem->key = 2;
		$expectedItem->value = 7;
		$o->pop();

		$this->assertSame(3, $countEvents);
	}

	public function testSort() {
		$o = ArrayObject::create([1, 2, 3]);

		$countEvents = 0;
		$listener = function($e) use(&$countEvents, $o) {
			$this->assertTrue($e->getTarget() === $o);

			$countEvents++;
		};
		$o->getEventManager()->attach(ArrayObject::EVENT_SORT, $listener);

		$o->multisort();
		$o->sort();
		$o->shuffle();

		$this->assertSame(3, $countEvents);
	}

	public function testClone() {
		$o1 = ArrayObject::create([1, 2, 3]);

		$countEvents = 0;
		$listener = function($e) use(&$countEvents, $o1) {
			$this->assertTrue($e->getTarget() === $o1);

			$countEvents++;
		};
		$o1->getEventManager()->attach(ArrayObject::EVENT_ITEM_ADDED, $listener);

		$o2 = clone $o1;

		$o1['a'] = 'aaa';
		$this->assertSame('aaa', $o1['a']);
		$this->assertFalse(isset($o2['a']));
		$o2['a'] = 'aaa';

		$this->assertSame(1, $countEvents);
	}
}
