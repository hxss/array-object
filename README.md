# ArrayObject

The OOP interface for PHP array.

The class realize almost [all php functions](https://www.php.net/manual/en/ref.array.php).

## Basic principles of work:

* All php functions with `array_` prefix are now class methods without the prefix in camel case(`array_change_key_case -> ArrayObject::changeKeyCase()`);
* Class methods do not require `array` type arg in most cases;
* All methods that accepted `array` args now accepts `array|ArrayObject`;
* All methods that had to return `array` now returns `ArrayObject`;

## Exceptions and differences with php api:

### Not realized functions:

* [compact](https://www.php.net/manual/en/function.compact.php);
* [extract](https://www.php.net/manual/en/function.extract.php);
* [list](https://www.php.net/manual/en/function.list.php);

### Functions with changed API:

* `ArrayObject::combine()` - static method;
* `array_fill` & `array_fill_keys` are merged into static `ArrayObject::fill()` where second argument defines behavior;
* `ArrayObject::range()` - static method;
* All `array_diff*` functions merged into `ArrayObject::diff()` where userfunc always gets 4 args(key1, val1, key2, val2);
* `ArrayObject::filter()` always pass 2 args into userfunc: key and val;
* All `array_​intersect*` functions merged into `ArrayObject::intersect()` where userfunc always gets 4 args(key1, val1, key2, val2);
* `array_​key_​first` realized as `ArrayObject::firstKey()`;
* `array_​key_​last` realized as `ArrayObject::lastKey()`;
* All `array_​merge*` functions merged into `ArrayObject::merge()` method;
* `ArrayObject::multisort()` saves sorted arrays only in passed `ArrayObject` values. All `array`-type args will used in sorting as usual but wouldn't updated.
* `array_​rand` realized as `ArrayObject::randKeys()`;
* `array_replace` & `array_​replace_​recursive` are merged into static `ArrayObject::replace()` where last optional argument `true` means recursion;
* All array sort functions merged into `ArrayObject::sort()` with 3 new flags that can be combined with default flags(bitwise OR);
* `array_walk` & `array_walk_​recursive` are merged into static `ArrayObject::walk()` where last optional argument `true` means recursion;

### New features:

* `ArrayObject::recursive()` - recursively convert all sub-array in `ArrayObject`;
* `ArrayObject::items()` - returns original array;
* `ArrayObject::append()` - append one or more elements onto the end of array. Like `push` but returns self;
* `ArrayObject::prepend()` - Prepend one or more elements to the beginning of an array. Like `unshift` but returns self;
* `ArrayObject::rand()` - returns associated array with one or more random items of an array;
* `ArrayObject::first()` - returns first value of the array;
* `ArrayObject::last()` - returns last value of the array;
* `ArrayObject::eq()` - checks if the object equal to `$other` array;
* `ArrayObject::touch()` - returns the value at specified offset or init it with `$default` value;
* `ArrayObject::generator()` - generates new array by user generator;
